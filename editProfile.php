<?php
include_once('./includes/settings.inc.php');
include_once('./includes/connection.php');
include_once('./includes/functions.inc.php');
start_uControl();
logged_in();
buld_permissions();
page_permissions(0, 'edit-profile');
include_once('./designs/header.php');

if(isset($_POST['change']))
{	//Update Profile
	if(($_POST['username'] == NULL) || ($_POST['email'] == NULL) || ($_POST['fullname'] == NULL) || ($_POST['firstaid'] == NULL))
	{
		$eCheck = true;
	}
	
	if(!isset($eCheck))
	{
		$sql = "UPDATE users SET `username` = '" . mysql_real_escape_string($_POST['username']) . "'";
		if($_POST['password'] != NULL){$sql .= ", `password` = '" . hashPassword($_POST['password']) . "'";}
		$sql .= ", `email` = '" . mysql_real_escape_string($_POST['email']) . "', `fullname` = '" . mysql_real_escape_string($_POST['fullname']) . "', `firstaid` = '" . mysql_real_escape_string($_POST['firstaid']) . "' WHERE `id` = '" . $site['user']['id'] . "'";
		
		$rs = mysql_query($sql, $conn);
		echo('<br />');
		box_t('Profile');
		?>
		Profile has been updated.
        <meta http-equiv="refresh" content="2;url=<?php echo($site['url_path']); ?>/editProfile.php" />
		<?php
		box_b();
	}
}

?>
<br />
<table align="center" width="60%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?php if($site['user']['permissions'][0]['view-profiles']){echo('<a href="' . $site['url_path'] . '/viewProfile.php?id=' . $site['user']['id'] . '" alt="View Profile" title="View Profile" class="profile" id="view-profile">View Profile</a>');}else{echo('&nbsp;');} ?></td>
		<td><?php if($site['user']['permissions'][0]['edit-profile']){echo('<a href="' . $site['url_path'] . '/editProfile.php" alt="Edit Profile" title="Edit Profile" class="profile" id="edit-profile">Edit Profile</a>');}else{echo('&nbsp;');} ?></td>
		<td></td>
		<td><?php if($site['user']['permissions'][0]['view-members']){echo('<a href="' . $site['url_path'] . '/members.php" alt="View Members" title="View Members" class="members" id="view-members">View Members</a>');}else{echo('&nbsp;');} ?></td>
	</tr>
</table>
<br />
<br />
<?php

if(isset($_GET['na']))
{
}
else
{	//Main Page
	$rs = mysql_query("SELECT * FROM users WHERE `id` = '" . $site['user']['id'] . "'", $conn);
	$row = mysql_fetch_array($rs);
	?>
	<table align="center" width="45%" border="0" cellspacing="0" cellpadding="0">
    	<form action="<?php echo($site['url_path']); ?>/editProfile.php" method="post" name="editProfile">
		<tr>
        	<td colspan="2"><b>Edit Profile</b></td>
        </tr>
        <tr>
        	<td colspan="2">&nbsp;</td>
        </tr>
        <tr>
			<td>Username:</td>
			<td><input type="text" name="username" id="username" value="<?php echo($row['username']); ?>" readonly="true"></td>
		</tr>
		<tr>
			<td>Password:</td>
			<td><input type="text" name="password" id="password"></td>
		</tr>
		<tr>
			<td>Email:</td>
			<td><input type="text" name="email" id="email" value="<?php echo($row['email']); ?>"></td>
		</tr>
        <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Full Name:</td>
			<td><input type="text" name="fullname" id="fullname" value="<?php echo($row['fullname']); ?>"></td>
		</tr>
        <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
        <tr>
			<td>First Aid:</td>
			<td>
            <select name="firstaid" id="firstaid">
            <?php
			$rs2 = mysql_query("SELECT * FROM firstaid WHERE `status` = '1' ORDER BY `id` ASC", $conn);
			while($row2 = mysql_fetch_array($rs2))
			{
				echo('<option value="' . $row2['id'] . '"');
				if($row2['id'] == $row['firstaid']){echo(' selected');}
				echo('>' . $row2['name'] . '</option>');
			}
			?>
            </select>
            </td>
		</tr>
        <tr>
			<td>Membership Type:</td>
			<td>
            <select name="mType" id="mType">
            <?php
			$rs3 = mysql_query("SELECT * FROM mTypes WHERE `status` = '1' ORDER BY `id` ASC", $conn);
			while($row3 = mysql_fetch_array($rs3))
			{
				echo('<option value="' . $row3['id'] . '"');
				if($row3['id'] == $row['mType']){echo(' selected');}
				echo('>' . $row3['name'] . '</option>');
			}
			?>
            </select>
          </td>
		</tr>
        <tr>
        	<td>MMS Number:</td> 
        	<td><input type="text" name="mmsn" id="mmsn" value="<?php echo($row['mmsn']); ?>" readonly /></td>
        </tr>
        <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><input type="submit" name="change" id="change" value="Change" /></td>
			<td>&nbsp;</td>
		</tr>
        </form>
	</table>
    <?php
}

footer_b();
?>