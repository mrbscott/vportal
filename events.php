<?php
include_once('./includes/settings.inc.php');
include_once('./includes/connection.php');
include_once('./includes/functions.inc.php');
start_uControl();
logged_in();
buld_permissions();
page_permissions(0, 'view-events');
$calButton = true;
include_once('./designs/header.php');

if($site['events'])
{	//Are we online?
	if(isset($_POST['create']))
	{
		//Lets check the content before we go ahead with adding it to the db
		$name = mysql_real_escape_string($_POST['name']);
		$type = mysql_real_escape_string($_POST['type']);
		$sDate = mysql_real_escape_string($_POST['sDate']);
		$eDate = mysql_real_escape_string($_POST['eDate']);
		$mType = mysql_real_escape_string($_POST['mType']);
		$details = mysql_real_escape_string($_POST['details']);
		$org = mysql_real_escape_string($_POST['organizer']);
		$par = mysql_real_escape_string($_POST['participants']);
		echo('<br />');
		box_t('Events');
		
		if(($name == NULL) || ($type == NULL) ||  ($sDate == NULL) || ($eDate == NULL) || ($mType == NULL) || ($details == NULL) || ($org == NULL) || ($par == NULL))
		{
			echo('You cannot leave any field blank.<br />');
			$eCheck = true;
		}
		
		$mkToday = mktime(0,0,0,date('m'), date('d'), date('Y'));
		
		$startDate = explode('/', $sDate);
		$mkSdate = mktime(0,0,0,$startDate[0],$startDate[1],$startDate[2]);
		if(($startDate[0] > 12) || ($startDate[1] > 31) || ($startDate[2] < date('Y')) || ($startDate[2] > date('Y')+5) || (strlen($sDate) != 10) || ($mkSdate <= $mkToday))
		{
			print_r($startDate);
			echo($sDate . ' - ');
			echo('Start date must be entered in correctly.<br />');
			$eCheck = true;
		}
		
		$endDate = explode('/', $eDate);
		$mkEdate = mktime(0,0,0,$endDate[0],$endDate[1],$endDate[2]);
		if(($endDate[0] > 12) || ($endDate[1] > 31) || ($endDate[2] < date('Y')) || ($endDate[2] > date('Y')+5) || (strlen($eDate) != 10)  || ($mkEdate <= $mkToday))
		{
			echo('End date must be entered in correctly.<br />');
			$eCheck = true;
		}
		
		if($mkSdate > $mkEdate)
		{
			echo('The end date cannot come before the start date.<br />');
			$eCheck = true;
		}
		
		if(($site['user']['level'] == 1) && ($site['user']['id'] != $org))
		{
			echo('How could you lie about who you are?');
			$eCheck = true;
		}
		
		if(!isset($eCheck))
		{	//Nothing failed...
			$sql = "INSERT INTO events (`name`, `type`, `startDate`, `endDate`, `mType`, `details`, `org`, `par`) VALUES ('" . $name . "', '" . $type . "', '" . $sDate . "', '" . $eDate . "', '" . $mType . "', '" . $details . "', '" . $org . "', '" . $par . "')";
			$rs = mysql_query($sql, $conn);
			
			echo('Event has been added.');
			
			$rs = mysql_query("SELECT `id` FROM events WHERE `name` = '" . $name . "' AND `startDate` = '" . $sDate . "'", $conn);
			$row = mysql_fetch_array($rs);
			echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/events.php?view=' . $row['id'] . '" /> ');
		}
		
		box_b();
		footer_b();
		exit;
	}
	elseif(isset($_GET['view']))
	{
		if((isset($_GET['remove'])) && (isset($_GET['par'])) && ($site['user']['level'] > 1))
		{	//If admin and wants to remove someone... let them go ahead...
			$rs = mysql_query("DELETE FROM evnPar WHERE `id` = '" . mysql_real_escape_string($_GET['remove']) . "'", $conn);
		}
		
		if((isset($_GET['remove-e'])) && ($site['user']['level'] > 1))
		{	//If admin and wants to remove the event... let them go ahead...
			$rs = mysql_query("DELETE FROM events WHERE `id` = '" . mysql_real_escape_string($_GET['view']) . "'", $conn);
			$rs = mysql_query("DELETE FROM evnPar WHERE `eventId` = '" . mysql_real_escape_string($_GET['view']) . "'", $conn);
		}
		
		if((isset($_GET['bo'])) && (isset($_GET['as'])))
		{	//Removes someone who has backed out...
			$rs = mysql_query("DELETE FROM evnPar WHERE `eventId` = '" . mysql_real_escape_string($_GET['view']) . "' AND `userId` = '" . $site['user']['id'] . "'", $conn);
		}
		
		if((isset($_GET['go'])) && (isset($_GET['as'])))
		{	//Adds someone who decideds to join the party
			$rs = mysql_query("INSERT INTO evnPar (`eventId`, `userId`) VALUES ('" . mysql_real_escape_string($_GET['view']) . "', '" . $site['user']['id'] . "')", $conn);
			echo('<meta http-equiv="refresh" content="0;url=' . $site['url_path'] . '/events.php?view=' . $_GET['view'] . '" /> ');	
		}
	}

	if($site['user']['permissions'][0]['create-events'])
	{
		?>
		<table align="center" width="80%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="80%">&nbsp;</td>
				<td><a href="<?php echo($site['url_path']); ?>/events.php?create" style="text-decoration:none;"><img src="<?php echo($site['url_path']); ?>/images/icons/add.gif" border="0"> Create Event</a></td>
			</tr>
		</table>
		<br />
		<?php
	}
	

	if(isset($_GET['create']))
	{	//Form
		?>
		<table align="center" width="65 %" border="0" cellspacing="0" cellpadding="0">
        <form id="createEvent" name="createEvent" method="post" action="<?php echo($site['url_path']); ?>/events.php?create">
			<tr>
				<td colspan="2"><b>Create Event</b></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td>Event Name:</td>
				<td><input type="text" name="name" id="name" /></td>
			</tr>
			<tr>
				<td>Event Type:</td>
				<td>
					<select name="type" id="type">
					<?php
					$rs = mysql_query("SELECT * FROM evnType WHERE `status` = '1'", $conn);
					while($row = mysql_fetch_array($rs))
					{
						echo('<option value="' . $row['id'] . '">' . $row['name'] . '</option>');
					}
					?>
					</select>
				</td>    
			</tr>
			<tr>
				<td>Start Date:</td>
				<td><input type="text" name="sDate" id="sDate" /> <img height="13" width="13" alt="Select Date" title="Select Date" onclick="displayDatePicker('sDate', this);" src="<?php echo($site['url_path']); ?>/images/icons/date-button.gif" /> <i>(mm/dd/yyyy)</i></td>
			</tr>
			<tr>
				<td>End Date:</td>
				<td><input type="text" name="eDate" id="eDate" /> <img height="13" width="13" alt="Select Date" title="Select Date" onclick="displayDatePicker('eDate', this);" src="<?php echo($site['url_path']); ?>/images/icons/date-button.gif" /> <i>(mm/dd/yyyy)</i></td>
			</tr>
			<tr>
				<td>Details:</td>
				<td><textarea name="details" id="details" cols="45" rows="5"></textarea></td>
			</tr>
            <tr>
				<td>Member Type:</td>
				<td>
					<select name="mType" id="mType">
                    	<option value="0">Everyone</option>
						<?php
                        $rs2 = mysql_query("SELECT `id`, `name` FROM mTypes", $conn);
                        while($row2 = mysql_fetch_array($rs2))
                        {
                            echo('<option value="' . $row2['id'] . '"');
                            if($row2['id'] == $site['user']['mType']){echo(' selected');}
                            echo('>' . $row2['name'] . '</option>');
                        }
                        ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Organizer:</td>
				<td>
				<?php
				if($user['level'] == 1)
				{
					?>
						<input name="org" type="text" id="org" value="<?php echo($user['fullname']); ?>" readonly="true" />
						<input name="organizer" type="hidden" id="organizer" value="<?php echo($site['user']['id']); ?>" readonly="true" />
					<?php
				}
				else
				{
					?>
					<select name="organizer" id="organizer">
					<?php
					$rs3 = mysql_query("SELECT `id`, `fullname` FROM users", $conn);
					while($row3 = mysql_fetch_array($rs3))
					{
						echo('<option value="' . $row3['id'] . '"');
						if($row3['id'] == $site['user']['id']){echo(' selected');}
						echo('>' . $row3['fullname'] . '</option>');
					}
					?>
					</select>
					<?php
				}
				?>
				</td>
			</tr>
			<tr>
				<td>Participants:</td>
				<td><input type="text" name="participants" id="participants" /> <i>(#)</i></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="create" id="create" value="Create" /></td>
			</tr>
        </form>
		</table>
		<?php
	}
	elseif(isset($_GET['view']))
	{	//View Event
		$rs = mysql_query("SELECT * FROM events WHERE `id` = '" . mysql_real_escape_string($_GET['view']) . "'", $conn);
		
		if(mysql_num_rows($rs) > 0)
		{
			$row = mysql_fetch_array($rs);
			
			$rs4 = mysql_query("SELECT `fullname` FROM users WHERE `id` = '" . $row['org'] . "'", $conn);
			$row4 = mysql_fetch_array($rs4);
			
			$rs5 = mysql_query("SELECT `name` FROM evnType WHERE `id` = '" . $row['type'] . "'", $conn);
			$row5 = mysql_fetch_array($rs5);
			
			//Show the damn event
			?>
			<table  align="center" width="55%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2">
						<font class="events" id="title"><?php echo($row['name']); ?> </font>
						<?php
						if($site['user']['permissions'][1]['remove-event'])
						{
							echo('<a href="' . $site['url_path'] . '/events.php?view=' . $row['id'] . '&remove-e"><img src="' . $site['url_path'] . '/images/icons/delete.gif" title="Remove" alt="Remove" border="0"></a>');
						}
						?>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">Event Type: <?php echo($row5['name']); ?></td>
                </tr>
                <tr>
					<td colspan="2"><i>Organized By: <b><a href="<?php echo($site['url_path'] . '/viewProfile.php?id=' . $row['org']); ?>" alt="<?php echo($row4['fullname']); ?>"><?php echo($row4['fullname']); ?></a></b></i></td>
				</tr>
                <tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">Start Date: <b><?php echo($row['startDate']); ?></b></td>
				</tr>
				<tr>
					<td colspan="2">End Date: <b><?php echo($row['endDate']); ?></b></td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">Details</td>
                </tr>
                <tr>
                	<td width="22%">&nbsp;</td>
					<td><?php echo($row['details']); ?></td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td>Participants (<?php echo($row['par']); ?>)
					<br />
					<br />
					<?php 
					$rs2 = mysql_query("SELECT `id`, `userId` FROM evnPar WHERE `eventId` = '" . $row['id'] . "'", $conn);
					
					if($row['par'] == mysql_num_rows($rs2))
					{	//Are we full?
						echo('<b>Full</b>');
						$eFull = true;
					}
					
					$rs4 = mysql_query("SELECT `id` FROM evnPar WHERE `eventId` = '" . $row['id'] . "' AND `userId`='" . $site['user']['id'] . "'", $conn);
					if((!isset($eFull)) && (mysql_num_rows($rs4) == 0))
					{	//Have they already joined?
						echo('<a href="events.php?view=' . $row['id'] . '&as=' . $site['user']['id'] . '&go" alt="Sign Up (Go to the event...)">Sign Up</a>');
						$join = true;
					}
					
					$sD = explode('/', $row['startDate']);
					$mkDate = mktime(0,0,0,$sD[0],$sD[1],$sD[2]) - mktime(0,0,0,date('m'),date('d'),date('Y'));
					if((!isset($join)) && ($mkDate>(86400*3)))
					{	//Get out!
						if(isset($eFull))
						{
							echo('<br /><br />');
						}
					
						echo('<a href="events.php?view=' . $row['id'] . '&as=' . $site['user']['id'] . '&bo" alt="Backout!!">Backout</a>');
					}
					?>
					</td>
					<td>
					<?php
					if($row2 = mysql_fetch_array($rs2))
					{
						$x++;
						
						$rs3 = mysql_query("SELECT `fullname` FROM users WHERE `id` = '" . $row2['userId'] . "'", $conn);
						$row3 = mysql_fetch_array($rs3);
						
						if($site['user']['permissions'][1]['remove-event'])
						{	//If admin
							echo($row3['fullname'] . ' <a href="' . $site['url_path'] . '/events.php?view=' . $row['id'] . '&remove=' . $row2['id'] . '&par"><img src="' . $site['url_path'] . '/images/icons/delete.gif" title="Remove" alt="Remove" border="0"></a>');
						}
						else
						{	//If norm user
							echo($row3['fullname']);
						}
						
						if($x != mysql_num_rows($rs2))
						{
							echo(', ');
						}
					}
					else
					{
						echo('No one has signed up!');
					}
					?>
					</td>
				</tr>
			</table>
			</center>
			<?php
		}
		else
		{
			//Nothing found. Sorry.
			echo('<br /><br />');
			box_t('Events');
			?>
			No event found.
			<meta http-equiv="refresh" content="2;url=<?php echo($site['url_path']); ?>/events.php" />
			<?php
			box_b();
		}
	}
	else
	{
		$rs = mysql_query("SELECT `id`, `startDate` FROM events", $conn);
	
		if(mysql_num_rows($rs) > 0)
		{	//list
			while($row = mysql_fetch_array($rs))
			{
				$sDate = explode('/', $row['startDate']);
				
				$mkSdate = mktime(0,0,0,$sDate[0],$sDate[1],$sDate[2]);
				$mkToday = mktime(0,0,0,date('m'),date('d'),date('Y'));
				if($mkToday > $mkSdate){$rs98 = mysql_query("DELETE FROM events WHERE `id`='" . $row['id'] . "'", $conn);$rs99 = mysql_query("DELETE FROM evnPar WHERE `eventId`='" . $row['id'] . "'", $conn);echo('<meta http-equiv="refresh" content="0;url=' . $site['url_path'] . '/events.php" /> ');	}
				
				if(($sDate[0] == '12') || ($sDate[0] == '01') || ($sDate[0] == '02'))
				{	//Winter
					$a++;
					$section[1][$a] = $row['id'];
				}
				elseif(($sDate[0] == '03') || ($sDate[0] == '04') || ($sDate[0] == '05')  || ($sDate[0] == '06'))
				{	//Spring
					$b++;
					$section[2][$b] = $row['id'];
				}
				elseif(($sDate[0] == '07') || ($sDate[0] == '08') || ($sDate[0] == '09'))
				{	//Summer
					$c++;
					$section[3][$c] = $row['id'];
				}
				elseif(($sDate[0] == '10') || ($sDate[0] == '11'))
				{	//Fall
					$d++;
					$section[4][$d] = $row['id'];
				}
			}
			
			//Sections
			// Winter (Dec12-Feb2)
			// Spring (Mar3-June6)
			// Summer (Jul7-Sept9)
			// Fall (Oct10-Nov11)
			$s[1] = 'Winter';
			$s[2] = 'Spring';
			$s[3] = 'Summer';
			$s[4] = 'Fall';
			
			for($i=1; $i<=4; $i++)
			{
				if(sizeof($section[$i]) > 0){if($i!=1){echo('<br />');} echo('<div class="events" id="section-title">' . $s[$i] . '</div><br />');}
				for($x=1; $x<=sizeof($section[$i]); $x++)
				{
					$rs = mysql_query("SELECT * FROM events WHERE `id` = '" . $section[$i][$x] . "'", $conn);
					$row = mysql_fetch_array($rs);
				
					$rs2 = mysql_query("SELECT `fullname` FROM users WHERE `id` = '" . $row['org'] . "'", $conn);
					$row2 = mysql_fetch_array($rs2);
				
					$rs3 = mysql_query("SELECT `name` FROM evnType WHERE `id` = '" . $row['type'] . "'", $conn);
					$row3 = mysql_fetch_array($rs3);
				
					?>
					<center>
                    <div class="events round" id="oBox">
                        <div class="events" id="iBox">
                            <div class="events" id="title"><a href="<?php echo($site['url_path'] . '/events.php?view=' . $row['id'] . '" alt="' . $row['name'] . '">' . $row['name']); ?></a></div>
                            <div class="events" id="org">Organized By: <a href="<?php echo($site['url_path'] . '/viewProfile.php?id=' . $row['org'] . '" alt="'. $row2['fullname'] . '">' . $row2['fullname']); ?></a></div>
                            <div class="events" id="type">Event Type: <?php echo($row3['name']); ?></div>
                            <div class="dummy"></div>
                            <!--<div>&nbsp;</div>-->
                            <div class="events" id="sDate">Start Date: <?php echo($row['startDate']); ?></div>
                            <div class="events" id="eDate">End Date: <?php echo($row['endDate']); ?></div>
                            <div class="dummy"></div>
                            <div>&nbsp;</div>
                            <div class="events" id="details">Details: <?php echo(substr($row['details'], 0, 55)); if(strlen($row['details'])>55){echo('...');}?></div>
                        </div>
                    </div>
                    </center>
					<br />
					<?php
				}
			}
		}
		else
		{	//Nothing found. Sorry.
			box_t('Events');
			?>
			No events found.
			<?php
			box_b();
		}
	}
}
else
{	//Events is offline...
	echo('<br /><br />');
	box_t('Events');
	?>
	The events area is currently offline.
	<?php
	box_b();
}
footer_b();
?>