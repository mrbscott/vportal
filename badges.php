<?php
include_once('./includes/settings.inc.php');
include_once('./includes/connection.php');
include_once('./includes/functions.inc.php');
start_uControl();
logged_in();
buld_permissions();
page_permissions(0, 'view-badges');
include_once('./designs/header.php');

echo('<br /><br />');
if($site['badges'])
{	//Are we online?
	if(isset($_GET['remove']))
	{
		$rs = mysql_query("DELETE FROM badgeTracking WHERE `id` = '" . cleanString($_GET['remove'], 'text-input') . "' AND `userId` = '" . $site['user']['id'] . "'", $conn);
	}
	elseif(isset($_POST['create']))
	{
		if($_POST['badge'] == NULL)
		{
			$eCheck = true;
		}
		
		if(!isset($eCheck))
		{
			$rs = mysql_query("INSERT INTO badgeTracking (`userId`, `badgeId`, `startDate`) VALUES ('" . $site['user']['id'] . "', '" . cleanString($_POST['badge'], 'text-input') . "', '" . date('m/d/Y') . "')", $conn);
			
			$rs = mysql_query("SELECT `id` FROM badgeTracking WHERE `userId` = '" . $site['user']['id'] . "' AND `badgeId` = '" . cleanString($_POST['badge'], 'text-input') . "'", $conn);
			$row = mysql_fetch_array($rs);
	
			box_t('Badges');
			echo('Badge work has been created.');
			echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/badges.php?view=' . $row['id'] . '" /> ');
			box_b();
		}
		else
		{
			box_t('Badges');
			echo('Badge work could not be created.');
			box_b();
		}
	}
	elseif(isset($_POST['change']))
	{
		if($_POST['id'] != NULL)
		{
			$rs = mysql_query("SELECT `id`, `badgeId` FROM badgeTracking WHERE `id` = '" . mysql_real_escape_string($_POST['id']) . "' AND `userId` = '" . $site['user']['id'] . "'", $conn);
			if(mysql_num_rows($rs))
			{
				$row = mysql_fetch_array($rs);
				$rs2 = mysql_query("SELECT `program` FROM badges WHERE `id` = '" . $row['badgeId'] . "'", $conn);
				$row2 = mysql_fetch_array($rs2);
				
				$program = explode('[*]', $row2['program']);
				$tp1 = sizeof($program)-1;
				$x=0;
				for($i=0; $i<=$tp1; $i++)
				{
					if($_POST['p-' . $i]){$progress .= '1';$x++;}else{$progress .= '0';}
				}
				
				$sql = "UPDATE badgeTracking SET `progress` = '" . $progress . "'";
				if($x==sizeof($program)){$sql .= ", `finished` = '1', `endDate` = '" . date('m/d/Y') . "'";}
				$sql .= " WHERE `id` = '" . $row['id'] . "'";
				$rs = mysql_query($sql, $conn);
				
				$updated = true;
			}
		}
	}
	
	if(isset($updated))
	{
		box_t('Badges');
		echo('Badge work has been updated.');
		echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/badges.php" /> ');
		box_b();
	}
	?>
    <table align="center" width="75%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="80%">&nbsp;</td>
			<td><?php if($site['user']['permissions'][0]['start-badge-work']){echo('<a href="' . $site['url_path'] . '/badges.php?new" alt="New Badge!" title="New Badge!" "  class="badges" id="new-badge"><img src="' . $site['url_path'] . '/images/icons/add.gif" border="0"> New Badge</a>');} ?></td>
		</tr>
	</table>
    <br />
    <?php
	if(isset($_GET['new']))
	{
		$rs = mysql_query("SELECT `name` FROM mTypes WHERE `id` = '" . $site['user']['mType'] . "'", $conn);
		$row = mysql_fetch_array($rs);
		?>
        <table align="center" width="35%" border="0" cellspacing="0" cellpadding="0">
        	<form action="<?php echo($site['url_path']); ?>/badges.php" method="post" name="u-addBadge">
			<tr>
				<td colspan="2"><b>Add New Badge</b></td>
			</tr>
            <tr>
            	<td colspan="2">&nbsp;</td>
            </tr>
			<tr>
				<td>Section:</td>
				<td><input type="text" name="section" id="section" value="<?php echo($row['name']); ?>" readonly="readonly" /></td>
			</tr>
            <tr>
				<td>Badge:</td>
				<td>
                    <select name="badge" id="badge">
                    <?php
                    $rs2 = mysql_query("SELECT `id`, `name` FROM badges WHERE `mType` = '" . $site['user']['mType'] . "' ORDER BY `name` ASC", $conn);
                    
                    while($row2 = mysql_fetch_array($rs2))
                    {
                        echo('<option value="' . $row2['id'] . '">' . $row2['name'] . '</option>');
                    }
                    ?>
                    </select>
                </td>
			</tr>
            <tr>
            	<td colspan="2">&nbsp;</td>
            </tr>
            <tr>
            	<td colspan="2"><input type="submit" name="create" id="create" value="Create" /></td>
            </tr>
            </form>
		</table>
        <?php
	}
	elseif(isset($_GET['view']))
	{	//View SELECT badge work...
		$rs = mysql_query("SELECT * FROM badgeTracking WHERE `id` = '" . mysql_real_escape_string($_GET['view']) . "' AND `userId` = '" . $site['user']['id'] . "'", $conn);
		if(mysql_num_rows($rs))
		{
			$row = mysql_fetch_array($rs);
			
			$rs2 = mysql_query("SELECT * FROM badges WHERE `id` = '" . $row['badgeId'] . "'", $conn);
			$row2 = mysql_fetch_array($rs2);
                
            $rs3 = mysql_query("SELECT `name` FROM mTypes WHERE `id` = '" . $row2['mType'] . "'", $conn);
            $row3 = mysql_fetch_array($rs3);
            
			?>
            <table align="center" width="75%" border="0" cellspacing="0" cellpadding="0">
            	<form action="<?php echo($site['url_path']); ?>/badges.php" method="post" name="u-editBadge">
            	<tr>
                	<td colspan="2">
                    <b><?php echo($row2['name']); ?></b>
                    <input type="hidden" name="id" id="id" value="<?php echo($row['id']); ?>" />
                    </td>
                </tr>
                <tr>
                	<td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                	<td>Description</td>
                    <td><?php echo($row2['description']); ?></td>
                </tr>
                <tr>
                	<td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                	<td colspan="2">Program</td>
                </tr>
                <?php
				$progress = $row['progress'];
				$program = explode('[*]', $row2['program']);
				
				foreach($program as $key => $value)
				{
					?>
                    <tr>
                        <td><input type="checkbox" name="p-<?php echo($key); ?>"<?php if($progress[$key]){echo(' checked');}?> /><i>Completed</i></td>
                        <td><?php echo($value); ?></td>
                    </tr>
                    <?php
                }
				?>
                <tr>
                	<td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                	<td colspan="2"><input type="submit" name="change" id="change" value="Change" /></td>
                </tr>
                </form>
            </table>
            <?php
		}
		else
		{	//Nothing found...
			box_t('Badges');
			echo('No badge work found.');
			echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/badges.php" />');
			box_b();
		}
	}
	else
	{	//Main Page
		?>
        <table align="center" width="75%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="4"><div class="badges" id="unfinished-badges-title">Unfinished Badges</div></td>
            </tr>
            <tr>
                <td><b>Name</b></td>
                <td><b>Type</b></td>
                <td><b>Progress</b></td>
                <td>&nbsp;</td>
            </tr>
            <?php
            $rs = mysql_query("SELECT * FROM badgeTracking WHERE `userId` = '" . $site['user']['id'] . "' AND `endDate` = ''", $conn);
			if(mysql_num_rows($rs))
			{
				while($row = mysql_fetch_array($rs))
				{
					$rs2 = mysql_query("SELECT `name`, `mType` FROM badges WHERE `id` = '" . $row['badgeId'] . "'", $conn);
					$row2 = mysql_fetch_array($rs2);
					
					$rs3 = mysql_query("SELECT `name` FROM mTypes WHERE `id` = '" . $row2['mType'] . "'", $conn);
					$row3 = mysql_fetch_array($rs3);
					
					$progress = $row['progress'];
					for($i=0;$i<=strlen($progress);$i++){if($progress[$i] == 1){$x++;}}
					if(($x>0)&&(strlen($progress)>0)){$progress=round(($x*100)/strlen($progress));}else{$progress=0;}
					?>
					<tr>
						<td>
                        	<?php
                            if($site['user']['permissions'][0]['complete-badge-work'])
							{
								echo('<a href="' . $site['url_path'] . '/badges.php?view=' . $row['id'] . '" title="Work on ' . $row2['name'] . '" alt="Work on ' . $row2['name'] . '">' . $row2['name'] . '</a>');
							}
							else
							{
								echo($row2['name']);
							}
							?>
                        </td>
						<td><?php echo($row3['name']); ?></td>
						<td><?php echo($progress); ?>%</td>
						<td><?php if($site['user']['permissions'][0]['remove-badge']){echo('<a href="' . $site['url_path'] . '/badges.php?remove=' . $row['id'] . '"><img src="' . $site['url_path'] . '/images/icons/delete.gif" title="Remove" alt="Remove" border="0"></a>');}?></td>
					</tr>
					<?php
				}
			}
			else
			{
				$noBadgeWork = true;
			}
			?>
        </table>
        <br />
        <?php
		if(isset($noBadgeWork))
		{
			box_t('Badges');
			echo('No badge work found.<br />');
			if($site['user']['permissions'][0]['start-badge-work']){echo('Start a <a href="' . $site['url_path'] . '/badges.php?new" alt="New Badge!" title="New Badge!">new badge</a> today!');}
			box_b();
		}
		$rs = mysql_query("SELECT * FROM badgeTracking WHERE `userId` = '" . $site['user']['id'] . "' AND `endDate` != '' ORDER BY `progress` DESC", $conn);
		if(mysql_num_rows($rs))
		{
			?>
			<br />
			<table align="center" width="75%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="4"><div class="badges" id="finished-badges-title">Finished Badges</div></td>
				</tr>
				<tr>
					<td><b>Name</b></td>
					<td><b>Type</b></td>
					<td>&nbsp;</td>
				</tr>
				<?php
				while($row = mysql_fetch_array($rs))
				{
					$rs2 = mysql_query("SELECT `name`, `mType` FROM badges WHERE `id` = '" . $row['badgeId'] . "'", $conn);
					$row2 = mysql_fetch_array($rs2);
					
					$rs3 = mysql_query("SELECT `name` FROM mTypes WHERE `id` = '" . $row2['mType'] . "'", $conn);
					$row3 = mysql_fetch_array($rs3);
		
					?>
					<tr>
						<td><?php echo($row2['name']); ?></td>
						<td><?php echo($row3['name']); ?></td>
						<td><?php if($site['user']['permissions'][0]['remove-badge']){echo('<a href="' . $site['url_path'] . '/badges.php?remove=' . $row['id'] . '"><img src="' . $site['url_path'] . '/images/icons/delete.gif" title="Remove" alt="Remove" border="0"></a>');}?></td>
					</tr>
					<?php
				}
				?>
			</table>
			<?php
		}
	}
}
else
{	//I'm sorry... we are closed.
	box_t('Badges');
	?>
    Badges is currently offline.
    <?php
	box_b();
}
footer_b();
?>