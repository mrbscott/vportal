<?php
include_once('./includes/settings.inc.php');
include_once('./includes/connection.php');
include_once('./includes/functions.inc.php');
start_uControl();
logged_in();
buld_permissions();
page_permissions(0, 'view-threads');
include_once('./designs/header.php');
echo('<br /><br />');
if($site['forums'])
{
	if((isset($_GET['remove'])) && (isset($_GET['thread'])) && ($site['user']['permissions'][1]['remove-threads']))
	{	//Remove Thread	- Admin
		$rs = mysql_query("DELETE FROM threads WHERE `id` = '" . cleanString($_GET['remove'], 'text-input') . "'", $conn);
		$rs2 = mysql_query("DELETE FROM posts WHERE `threadId` = '" . cleanString($_GET['remove'], 'text-input') . "'", $conn);
		
		box_t('Forums');
		echo('Thread removed.');
		echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/forums.php" />');
		box_b();
		exit;
	}
	elseif((isset($_GET['remove'])) && (isset($_GET['thread'])) && ($site['user']['permissions'][0]['remove-threads']))
	{	//Remove Thread	- User
		$rs = mysql_query("SELECT `threadId` FROM posts WHERE `threadId` = '' AND `by` = '" . $site['user']['id'] . "'", $conn);
		$row = mysql_fetch_array($rs);
		
		if(mysql_num_rows($rs))
		{
			$rs2 = mysql_query("DELETE FROM threads WHERE `id` = '" . cleanString($_GET['remove'], 'text-input') . "'", $conn);
			$rs3 = mysql_query("DELETE FROM posts WHERE `threadId` = '" . cleanString($_GET['remove'], 'text-input') . "'", $conn);
		
			box_t('Forums');
			echo('Thread removed.');
			echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/forums.php" />');
			box_b();
			exit;
		}
	}
	elseif((isset($_GET['remove'])) && (isset($_GET['post'])) && ($site['user']['permissions'][1]['remove-posts']))
	{	//Remove Post - Admin
		$rs = mysql_query("DELETE FROM posts WHERE `id` = '" . cleanString($_GET['remove'], 'text-input') . "'", $conn);
		
		if(isset($_GET['first']))
		{
			$rs2 = mysql_query("DELETE FROM threads WHERE `id` = '" . cleanString($_GET['id'], 'text-input') . "'");
			$rs3 = mysql_query("DELETE FROM posts WHERE `threadId` = '" . cleanString($_GET['id'], 'text-input') . "'", $conn);
			
			box_t('Forums');
			echo('Thread removed.');
			echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/forums.php" />');
			box_b();
			exit;
		}
		
		box_t('Forums');
		echo('Post removed.');
		echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/threads.php?id=' . cleanString($_GET['id'], 'text-input') . '&post=' . cleanString($_GET['post'], 'text-input') . '" />');
		box_b();
		exit;
	}
	elseif((isset($_GET['remove'])) && (isset($_GET['post'])) && ($site['user']['permissions'][0]['remove-posts']))
	{	//Remove Post - User
		$rs = mysql_query("DELETE FROM posts WHERE `id` = '" . cleanString($_GET['remove'], 'text-input') . "' AND `by` = '" . $site['user']['id'] . "'", $conn);
		
		if(isset($_GET['first']))
		{
			$rs2 = mysql_query("DELETE FROM threads WHERE `id` = '" . cleanString($_GET['id'], 'text-input') . "' AND `by` = '" . $site['user']['id'] . "'");
			$rs3 = mysql_query("DELETE FROM posts WHERE `threadId` = '" . cleanString($_GET['id'], 'text-input') . "'", $conn);
			
			box_t('Forums');
			echo('Thread removed.');
			echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/forums.php" />');
			box_b();
			exit;
		}
		
		box_t('Forums');
		echo('Post removed.');
		echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/threads.php?id=' . cleanString($_GET['id'], 'text-input') . '&post=' . cleanString($_GET['post'], 'text-input') . '" />');
		box_b();
		exit;
	}
	elseif(isset($_POST['post-r']))
	{	//Post reply build script
		if(($_POST['content'] == NULL) || ($_GET['id'] == NULL) || ($_GET['post'] == NULL))
		{
			$eCheck = true;
		}
		
		if(!isset($eCheck))
		{
			$rs = mysql_query("INSERT INTO posts (`threadId`, `by`, `content`, `date`) VALUES ('" . cleanString($_GET['id'], 'text-input') . "', '" . $site['user']['id'] . "', '" . cleanString($_POST['content'], 'text-input') . "', '" . date('m/d/Y') . "')", $conn);
			
			$rs2 = mysql_query("SELECT `id` FROM posts WHERE `threadId` = '" . cleanString($_GET['id'], 'text-input') . "' AND `date` = '" . date('m/d/Y') . "'", $conn);
			$row2 = mysql_fetch_array($rs2);
		
			$rs3 = mysql_query("UPDATE threads SET `lastDate` = '" . date('m/d/Y') . "', `lastPost` = '" . $row2['id'] . "', `lastBy` = '" . $site['user']['id'] . "', `lastUpdated` = '" . mktime(0,0,0,date('m'),date('d'),date('Y')) . "' WHERE `id` = '" . cleanString($_GET['id'], 'text-input') . "'", $conn);
		
			box_t('Forums');
			echo('Post added.');
			echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/threads.php?id=' . cleanString($_GET['id'], 'text-input') . '&post=' . $row2['id'] . '" />');
			box_b();
		}
	}
	elseif(isset($_POST['change-p']))
	{	//Edit Post - Admin & User
		if($_POST['first'] == 1)
		{
			if($_POST['title'] == NULL)
			{
				$eCheck = true;
			}
			else
			{
				$first_p = true;
			}
		}
		
		if($_POST['content'] == NULL)
		{
			$eCheck = true;
		}
		
		if(!isset($eCheck))
		{
			$sql = "UPDATE posts SET `content` = '" . cleanString($_POST['content'], 'text-input') . "' WHERE `id` = '" . cleanString($_POST['post'], 'text-input') . "' AND `threadId` = '" . cleanString($_GET['id'], 'text-input') . "'";
			if(!$site['user']['permissions'][1]['edit-posts']){$sql .= " AND `by` = '" . $site['user']['id'] . "'";}
			$rs = mysql_query($sql, $conn);
			
			if(isset($first_p))
			{
				$sql = "UPDATE threads SET `title` = '" . cleanString($_POST['title'], 'text-input') . "'";
				if($_POST['forum'] != NULL){$sql .= ", `forumId` = '" . cleanString($_POST['forum'], 'text-input') . "'";}
				$sql .= " WHERE `id` = '" . cleanString($_POST['thread'], 'text-input') . "'";
				$rs = mysql_query($sql, $conn);
			}
			
			box_t('Forums');
			echo('Post changed.');
			echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/threads.php?id=' . cleanString($_GET['id'], 'text-input') . '&post=' . $row2['id'] . '" />');
			box_b();
		}
		else
		{
			box_t('Forums');
			echo('Post could not be changed.');
			box_b();
			echo('<br />');
		}
	}
	elseif(isset($_POST['create-t']))
	{
		if(($_POST['title'] == NULL) || ($_POST['fid'] == NULL) || ($_POST['content'] == NULL))
		{
			$eCheck = true;
		}
		
		if(!isset($eCheck))
		{
			$rs = mysql_query("INSERT INTO threads (`forumId`, `title`, `by`, `lastDate`, `lastBy`, `lastUpdated`) VALUES ('" . cleanString($_POST['fid'], 'text-input') . "', '" . cleanString($_POST['title'], 'text-input') . "', '" . $site['user']['id'] . "', '" . date('m/d/Y') . "', '" . $site['user']['id'] . "', '" . mktime(0,0,0,date('m'),date('d'),date('Y')) . "')", $conn);
			
			$rs2 = mysql_query("SELECT `id` FROM threads WHERE `forumId` = '" . cleanString($_POST['fid'], 'text-input') . "' AND `title` = '" . cleanString($_POST['title'], 'text-input') . "' AND `by` = '" . $site['user']['id'] . "'", $conn);
			$row2 = mysql_fetch_array($rs2);
			
			$rs3 = mysql_query("INSERT INTO posts (`threadId`, `by`, `content`, `date`) VALUES ('" . $row2['id'] . "', '" . $site['user']['id'] . "', '" . cleanString($_POST['content'], 'text-input') . "', '" . date('m/d/Y') . "')", $conn);
			
			$rs4 = mysql_query("SELECT `id` FROM posts WHERE `threadId` = '" . $row2['id'] . "' AND `by` = '" . $site['user']['id'] . "'", $conn);
			$row4 = mysql_fetch_array($rs4);
			
			$rs5 = mysql_query("UPDATE threads SET `lastPost` = '" . $row4['id'] . "' WHERE `forumId` = '" . cleanString($_POST['fid'], 'text-input') . "' AND `title` = '" . cleanString($_POST['title'], 'text-input') . "' AND `by` = '" . $site['user']['id'] . "'", $conn);
			
			//pff that was a lot of work...
			box_t('Forums');
			echo('Thread created.');
			echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/threads.php?id=' . $row2['id'] . '&post=' . $row4['id'] . '" />');
			box_b();
			exit;
		}
	}



	//SPACE



	if((isset($_GET['id'])) && (isset($_GET['post'])) && (isset($_GET['o'])))
	{	//View Thread from outside system
		echo('Coming Soon!');
	}
	elseif((isset($_GET['id'])) && (isset($_GET['o'])))
	{	//View Threads from outside system
		echo('Coming Soon!');
	}
	elseif((isset($_GET['create'])) && (isset($_GET['fid'])))
	{	//Create Thread
		$rs = mysql_query("SELECT `name`, `id` FROM forums WHERE `id` = '" . cleanString($_GET['fid'], 'text-input') . "'");
		
		if(mysql_num_rows($rs))
		{
			$row = mysql_fetch_array($rs);
			?>
			<table align="center" width="55%" border="0" cellspacing="0" cellpadding="0">
				<form method="post" action="<?php echo($site['url_path'] . '/threads.php?fid=' . cleanString($_GET['fid'], 'text-output')); ?>" name="u-createThread" id="u-createThread">
				<tr>
					<td colspan="2"><b>Create Thread</b></td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td>Thread Title:</td>
					<td><input type="text" name="title" id="title" /></td>
				</tr>
				<tr>
					<td>Forum:</td>
					<td>
                    	<input type="text" name="forum" id="forum" value="<?php echo($row['name']); ?>" readonly="readonly" />
                        <input type="hidden" name="fid" id="fid" value="<?php echo($row['id']); ?>" />
                    </td>
				</tr>
				<tr>
					<td>Message:</td>
					<td><textarea cols="45" rows="5" name="content" id="content"></textarea></td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" name="create-t" id="create-t" value="Create" /></td>
				</tr>
				</form>
			</table>
			<?php
		}
		else
		{	//No forum id found...
			box_t('Forums');
			echo('Forum could not be found.');
			echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/forums.php" />');
			box_b();
		}
	}
	elseif((isset($_GET['id'])) && (isset($_GET['post'])) && (isset($_GET['edit'])))
	{	//Edit Post
		if(($site['user']['permissions'][0]['edit-posts']) && (isset($_GET['first'])))
		{	//Is first post - now check!
			$rs = mysql_query("SELECT * FROM threads WHERE `id` = '" . cleanString($_GET['id'], 'text-input') . "' AND `by` = '" . $site['user']['id'] . "'");
			if(mysql_num_rows($rs)){$first_p = true;}
		}
		
		if($site['user']['permissions'][1][50])
		{
			$rs2 = mysql_query("SELECT * FROM posts WHERE `id` = '" . cleanString($_GET['edit'], 'text-input') . "'");
		}
		else
		{
			$rs2 = mysql_query("SELECT * FROM posts WHERE `id` = '" . cleanString($_GET['edit'], 'text-input') . "' AND `by` = '" . $site['user']['id'] . "'");
		}
		if(mysql_num_rows($rs2))
		{	//Found post...
			$row2 = mysql_fetch_array($rs2);
			?>
            <table align="center" width="55%" border="0" cellspacing="0" cellpadding="0">
				<form method="post" action="<?php echo($site['url_path'] . '/threads.php?id=' . cleanString($_GET['id'], 'text-output') . '&post=' . cleanString($_GET['post'], 'text-output')); ?>" name="u-editPost" id="u-editPost">
                <tr>
					<td colspan="2"><b>Edit Post</b></td>
				</tr>
                <tr>
					<td colspan="2">&nbsp;</td>
				</tr>
                <?php if((isset($first_p)) || ($site['user']['permissions'][1]['edit-posts']) && (isset($_GET['first']))){ $row = mysql_fetch_array($rs); ?>
				<tr>
					<td>Thread Title:</td>
					<td>
                    	<input type="text" name="title" id="title" value="<?php echo(cleanString($row['title'], 'text-output')); ?>" />
                    	<input type="hidden" name="first" id="first" value="1" />
                        <input type="hidden" name="thread" id="thread" value="<?php echo(cleanString($_GET['id'], 'text-output')); ?>" />
                    </td>
				</tr>
                <?php if($site['user']['permissions'][1][50]){ ?>
				<tr>
					<td>Forum:</td>
					<td>
                    	<select name="forum" id="forum">
                        	<?php
							$rs3 = mysql_query("SELECT `id`, `name` FROM forums WHERE `status` = '1'", $conn);
							while($row3 = mysql_fetch_array($rs3))
							{
								echo('<option value="' . $row3['id'] . '"');
								if($row3['id'] == $row['forumId']){echo(' selected');}
								echo('>' . $row3['name'] . '</option>');
							}
							?>
                        </select>
                    </td>
				</tr>
				<?php }echo('<tr><td colspan="2">&nbsp;</td></tr>');} ?>
                <tr>
					<td>Message:</td>
					<td>
                    	<textarea cols="45" rows="5" name="content" id="content"><?php echo(cleanString($row2['content'], 'text-output')); ?></textarea>
                        <input type="hidden" name="post" id="post" value="<?php echo($row2['id']); ?>" />
                    </td>
				</tr>
                <tr>
					<td colspan="2">&nbsp;</td>
				</tr>
                <tr>
					<td colspan="2"><input type="submit" name="change-p" id="change-p" value="Change" /></td>
				</tr>
                </form>
			</table>
            <?php
		}
		else
		{	//User does not have permission to edit.
			box_t('Forums');
			echo('You do not have permission to edit this post.');
			echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/forums.php" />');
			box_b();
		}
	}
	elseif((isset($_GET['id'])) && (isset($_GET['post'])))
	{	//View Thread - POST...
		$rs = mysql_query("SELECT * FROM threads WHERE `id` = '" . cleanString($_GET['id'], 'text-input') . "'", $conn);
		$rs2 = mysql_query("SELECT * FROM posts WHERE `threadId` = '" . cleanString($_GET['id'], 'text-input') . "' ORDER BY `id` ASC", $conn);
		
		if(mysql_num_rows($rs))
		{
			$row = mysql_fetch_array($rs);
			if(($site['user']['permissions'][1][52]) || ($site['user']['permissions'][0][21]) && ($row['by'] == $site['user']['id']))
			{
				echo('<div class="forums" id="thread-title-box-post-view"><div class="forums" id="thread-title-post-view-2">' . $row['title'] . '</div><div class="forums" id="thread-remove-post-view"><a href="' . $site['url_path'] . '/threads.php?remove=' . $row['id'] . '&thread"><img src="' . $site['url_path'] . '/images/icons/delete.gif" title="Remove" alt="Remove" border="0"></a></div><div class="dummy"></div></div><hr width="65%" />');
			}
			else
			{
				echo('<div class="forums" id="thread-title-box-post-view"><div class="forums" id="thread-title-post-view">' . $row['title'] . '</div><br /><hr /></div>');
			}
			
			if(mysql_num_rows($rs2))
			{
				while($row2 = mysql_fetch_array($rs2))
				{
					$x++;
					$rs3 = mysql_query("SELECT `fullname` FROM users WHERE `id` = '" . $row2['by'] . "'", $conn);
					$row3 = mysql_fetch_array($rs3);
					
					?>
					<center>
					<div class="forums" id="oBox-post-view">
						<div class="forums" id="iBox-post-view">
							<div class="forums" id="from-date-post-view"><?php if(mysql_num_rows($rs3)){echo('<a href="' . $site['url_path'] . '/viewProfile.php?id=' . $row2['by'] . '" alt="' . $row3['fullname'] . '" title="' . $row3['fullname'] . '">' . $row3['fullname'] . '</a>');}else{echo('Guest');} echo('<br />' . $row2['date']); ?></div>
							<div class="forums" id="content-post-view"><?php echo(cleanString($row2['content'], 'text-output')); ?></div>
							<div class="dummy"></div>
                           	<div class="forums" id="post-bottom-post-view">
								<?php
								if(($site['user']['permissions'][1]['edit-posts']) || ($site['user']['permissions'][0]['edit-posts']) && ($row2['by'] == $site['user']['id']))
								{
									echo('<a href="' . $site['url_path'] . '/threads.php?id=' . $row['id'] . '&post=' . cleanString($_GET['post'], 'text-output') . '&edit=' . $row2['id']); if($x==1){echo('&first');} echo('"><img src="' . $site['url_path'] . '/images/icons/edit.gif" title="Edit Post" alt="Edit Post" border="0"></a> ');
								}
								
                                if(($site['user']['permissions'][1]['remove-posts']) || ($site['user']['permissions'][0]['remove-posts']) && ($row2['by'] == $site['user']['id']))
                                {
                                    echo('<a href="' . $site['url_path'] . '/threads.php?id=' . $row['id'] . '&post=' . cleanString($_GET['post'], 'text-output') . '&remove=' . $row2['id'] . '&post'); if($x==1){echo('&first');} echo('"><img src="' . $site['url_path'] . '/images/icons/delete.gif" title="Remove Post" alt="Remove Post" border="0"></a>');
                                }						
                                ?>
                            </div>
						</div>
					</div>
					</center>
					<br />
					<?php
				}
				
				if($site['user']['permissions'][0]['relpy-to-threads'])
				{	//Can Reply
					?>
					<br />
					<br />
					<table align="center" width="45%" border="0" cellspacing="0" cellpadding="0">
						<form method="post" action="<?php echo($site['url_path']); ?>/threads.php?id=<?php echo($row['id'] . '&post=' . cleanString($_GET['post'], 'text-output')); ?>" name="u-replyThread" id="u-replyThread">
						<tr>
							<td>Message:</td>
							<td><textarea cols="45" rows="5" name="content" id="content"></textarea></td>
						</tr>
						<tr>
							<td colspan="2"><input type="submit" name="post-r" id="post-r" value="Relpy" /></td>
						</tr>
						</form>
					</table>
					<?php
				}
			}
			else
			{	//No posts found...
				box_t('Forums');
				echo('No posts found.');
				echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/forums.php" />');
				box_b();
			}
		}
		else
		{	//No thread found...
			box_t('Forums');
			echo('No thread found.');
			echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/forums.php" />');
			box_b();
		}
	}
	elseif(isset($_GET['fid']))
	{	//View Threads
		if($site['user']['permissions'][0]['create-threads'])
		{
			echo('<div class="forums" id="create-thread-view-threads"><a href="' . $site['url_path'] . '/threads.php?fid=' . cleanString($_GET['fid'], 'text-output') . '&create" style="text-decoration:none;" title="Create Thread" alt="Create Thread"><img src="' . $site['url_path'] . '/images/icons/add.gif" title="Create Thread" alt="Create Thread" border="0"> Create Thread</a></div><br />');
		}
		
		$rs = mysql_query("SELECT * FROM threads WHERE `forumId` = '" . cleanString($_GET['fid'], 'text-input') . "' ORDER BY `lastUpdated` ASC", $conn);
		
		if(mysql_num_rows($rs))
		{
			while($row = mysql_fetch_array($rs))
			{
				$rs2 = mysql_query("SELECT `fullname` FROM users WHERE `id` = '" . $row['lastBy'] . "'", $conn);
				$row2 = mysql_fetch_array($rs2);
				echo('<center><div class="forums" id="oBox-view-threads">');
				?>
				<div class="forums" id="iBox-view-threads">
					<div class="forums" id="title-view-threads">
						<?php echo('<a href="' . $site['url_path'] . '/threads.php?id=' . $row['id'] . '&post=' . $row['lastPost'] . '" alt="' . $row['title'] . '" title="' . $row['title'] . '" class="forums" id="title-view-threads-a">' . $row['title'] . '</a>'); ?>
					</div>
					<div class="forums" id="last-thread-group-view">
						<?php echo('<i>Last Post: ' . $row['lastDate'] . '<br /> <a href="' . $site['url_path'] . '/viewProfile.php?id=' . $row['lastBy'] . '" alt="' . $row2['fullname'] . '" title="' . $row2['fullname'] . '">' . $row2['fullname'] . '</a></i>'); ?>
					</div>
					<div class="dummy"></div>
				</div>
				<?php
				$x++;
				if(mysql_num_rows($rs)>$x){echo('<hr />');}
			}
			echo('</div></center>');
		}
		else
		{	//No threads found...
			box_t('Forums');
			echo('No threads found.');
			box_b();
		}
	}
	else
	{	//Nothing Found...
		box_t('Forums');
		echo('No threads found.');
		echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/forums.php" />');
		box_b();
	}
}
else
{	//Offline
	box_t('Forums');
	echo('Forums are currently offline.');
	box_b();
}
footer_b();
?>