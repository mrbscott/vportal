<?php
include_once('./includes/settings.inc.php');
include_once('./includes/connection.php');
include_once('./includes/functions.inc.php');

start_uControl();
if(isset($_POST['signin']))
{
	$site['status']['login'] = login_check();
}

if(isset($_GET['logout']))
{
	$site['status']['logout'] = logout();
}

include_once('./designs/header.php');
?>
<br />
<br />
<?php
box_t('Login');
if(isset($login))
{
	if(isset($login['s1']))
	{
		echo('You are already logged in.');
		echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/home.php" /> ');
	}
	if(isset($login['s2']))
	{
		echo('Login failed. Please try again.');
	}
	if(isset($login['s3']))
	{
		echo('Login failed. Please wait.');
	}
	if(isset($login['s4']))
	{
		echo('You are now logged in.');
		echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/home.php" /> ');
	}
}
elseif((isset($_GET['logout'])) && ($site['status']['logout']))
{
	echo('You have been logged out.');
	echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/login.php" /> ');
}
elseif(!$site['login'])
{
	echo('vPanel login is currently turned off.');
}
else
{
	?>	
	In order to gain access to the <?php echo($site['site_name']); ?> vPortal, you must have a valid username and password. Only members of <?php echo($site['site_name']); ?> are permitted to enter this area. Please enter your username and password below.
    <br />
    <br />
    <table width="75%" border="0" cellspacing="0" cellpadding="0">
	<form name="s-login" method="post" action="<?php echo($site['url_path']); ?>/login.php">	
    	<tr>			
			<td>Username:</td>
			<td><input type="text" name="username" id="username"></td>
        </tr>
        <tr>
			<td>Password:</td>
            <td><input type="password" name="password" id="password"></td>
        </tr>
        <tr>
        	<td colspan="2">
            	<input type="submit" name="signin" id="signin" value="Login">
            </td>
        </tr>
	</form>
    </table>
	<?php
}
box_b();
footer_b();
?>