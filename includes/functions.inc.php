<?php
function loadSettings()
{
	global $site, $conn;
	
	$rs = mysql_query("SELECT * FROM settings", $conn) or die('ENS');
	
	if(mysql_num_rows($rs))
	{
		while($row = mysql_fetch_array($rs))
		{
			$site[$row['name']] = $row['value'];
		}
	}
	else
	{
		echo('E404');
	}
}

function hashPassword($string)
{
	global $site;
	
	$string = sha1(md5(sha1($string . $site['keys']['password'])));
	
	return $string;
}

function mailAway($email, $title, $content)
{
	global $site;
	
	if($site['email-service'])
	{
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: ' . $site['email-nr'];
		$to = $email; 
		$subject = $title . ' - vPortal';
		$message = $content . '<br><br>Thanks,<br>' . $site['site_name'] . '<br>___<br>This message was intended for ' . $email . ' You can control emails by logging into your vPortal.';
		$mail_sent = @mail($to, $subject, $message, $headers);
		
		if(!$mail_sent)
		{
			return false;
		}
		else
		{
			return true;
		}	
	}
	else
	{
		return false;
	}
}

function login_check()
{
	//Get special vars
	global $site, $login, $conn;
	
	$failed = false;
	$status = false;
	
	//Check to see if they are already logged in.
	if((isset($_SESSION['user-auth'])) && ($_SESSION['user-auth']))
	{
		$login['s1'] = true;
		$failed = true;
	}
	
	//Check to see if they are coming from our servers.
	if(substr_count($_SERVER['HTTP_REFERER'], $site['url_path']) < 1)
	{
		$login['s2'] = true;
		$failed = true;
	}
	
	//Lets let them try to login five times before we cut them dry.
	if((isset($_SESSION['trys'])) && ($_SESSION['trys'] > 5))
	{
		$login['s3'] = true;
		$failed = true;
	}

	if(!$failed)
	{
		$username = mysql_real_escape_string($_POST['username']);
		$password = hashPassword($_POST['password']);
	
		if(($username != NULL) && ($password != NULL))
		{
			$rs = mysql_query("SELECT * FROM users WHERE `username` = '" . $username . "' AND `password` = '" . $password . "' AND `level` > '0'", $conn);
			
			if(mysql_num_rows($rs) == 1)
			{
				$row = mysql_fetch_array($rs);
				
				$_SESSION['user-auth'] = true;
				
				foreach($row as $key => $value)
				{
					if(!is_int($key))
					{
						$_SESSION['user-' . $key] = $value;
					}
				}
	
				$_SESSION['user-fingerprint'] = sha1($_SERVER['HTTP_USER_AGENT'] . $site['keys']['session']);
				
				$login['s4'] = true;
			}
			else
			{
				if(isset($_SESSION['trys']))
				{
					$_SESSION['trys'] = $_SESSION['trys'] + 1;
				}
				else
				{
					$_SESSION['trys'] = 1;
				}
				
				$login['s2'] = true;
			}
		}
	}
	
	if($failed)
	{
		$status = false;
	}

	return $status;
}

function login_review()
{
	global $site, $page;
	
	session_regenerate_id();
	
	if(isset($_SESSION['user-fingerprint']))
	{
		if($_SESSION['user-fingerprint'] == sha1($_SERVER['HTTP_USER_AGENT'] . $site['keys']['session']))
		{
			return true;
		}
	}
	return false;
}

function logout()
{
	global $site;
	
	if($site['user']['login-status'])
	{	
		session_destroy();
		return true;
	}
	return false;
}

function email_confirm($confirmK)
{
	//Get special vars
	global $site, $conn;
	
	$status = false;
	
	$emailCode = explode('-', $confirmK);
	$sql = ("SELECT * FROM confirmE WHERE `userId` = '" . $emailCode[0] . "' AND `code` = '" . $emailCode[1] . "' AND `key` = '" . $emailCode[2] . "'");
	$rs = mysql_query($sql, $conn);
	$numRows = mysql_num_rows($rs);
	
	if($numRows == 1)
	{
		$sql = "UPDATE users SET `level` = '" . $site['join-level'] . "' WHERE `id` = '" . $emailCode[0] . "'";
		$rs = mysql_query($sql, $conn);
		
		$sql = "DELETE FROM confirmE WHERE `userId` = '" . $emailCode[0] . "' AND `code` = '" . $emailCode[1] . "' AND `key` = '" . $emailCode[2] . "'";
		$rs = mysql_query($sql, $conn);
			
		$rs = mysql_query("SELECT `email` FROM users WHERE `id` = '" . $emailCode[0] . "'", $conn);
		$row = mysql_fetch_array($rs);
		
		$mail = mailAway($row['email'], 'Account Confirmed', 'Your account has now been confirmed and you may now login. You can login here; ' . $site['url_path'] . '/login.php');
			
		//Pass
		$status = true;
	}
	
	return $status;
}

function create_confirm($id, $email, $fullname)
{
	//Get special vars
	global $site, $signup, $conn;

	if($id == NULL)
	{
		return false;
	}
	else
	{	
		//Prep email confirm code...
		$code = substr(md5($id), 0, 8);
		$key = substr(md5($code), 0, 4);
		$sql = "INSERT INTO confirmE (`userId`, `code`, `key`) VALUES ('" . $id . "', '" . $code . "', '" . $key . "')";
		$rs = mysql_query($sql, $conn);
		$confirmCode = $id . '-' . $code . '-' . $key;
		
		if(mailAway($email, 'Account Confirmation', 'Please click on the link provided to confirm your account.<br><br> <a href="' . $site['url_path'] . '/signup.php?confirm=' . $confirmCode . '">Click Here</a>'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

function signup()
{
	//Get special vars
	$failed = false;
	$status = false;
	global $site, $signup, $conn;
	if($site['signup'])
	{
		$username = mysql_real_escape_string($_POST['username']);
		$password = hashPassword($_POST['password']);
		$email = mysql_real_escape_string($_POST['email']);
		$fullname = mysql_real_escape_string($_POST['fullname']);
		$firstaid = mysql_real_escape_string($_POST['firstaid']);
		$mType = mysql_real_escape_string($_POST['mType']);
		$ip = $_SERVER['REMOTE_ADDR'];
		
		//Check for missed spots
		if(($username == NULL) || ($password == NULL) || ($email == NULL) || ($fullname == NULL) || ($firstaid == NULL) || ($mType == NULL))
		{
			$failed = true;
			$signup['s2'] = true;
		}
		
		//Check to see if username is taken
		$sql = ("SELECT * FROM users WHERE `username` = '" . $username . "'");
		$rs = mysql_query($sql, $conn);
		if(mysql_num_rows($rs) > 0)
		{
			$failed = true;
			$signup['s3'] = true;
		}
		
		//Check to see if email is taken
		$sql = ("SELECT * FROM users WHERE `email` = '" . $email . "'");
		$rs = mysql_query($sql, $conn);
		if(mysql_num_rows($rs) > 0)
		{
			$failed = true;
			$signup['s4'] = true;
		}
		
		if(!$failed)
		{
			if(($site['join-aConfirm'] == true) || ($site['join-eConfirm'] == true)){$level = 6;}else{$level = $site['join-level'];}
			
			$sql = ("INSERT INTO users (`username`, `password`, `fullname`, `email`, `firstaid`, `mType`, `ip`, `level`, `joined`) VALUES ('" . $username . "', '" . $password . "', '" . $fullname . "', '" . $email . "', '" . $firstaid . "', '" . $mType . "', '" . $ip . "', '" . $level . "', '" . date('m/d/Y') . "')");
			$rs = mysql_query($sql, $conn);
			
			$sql = ("SELECT `id` FROM users WHERE `username` = '" . $username . "' AND `password` = '" . $password . "'");
			$rs = mysql_query($sql, $conn);
			$row = mysql_fetch_array($rs);
			
			if($site['join-eConfirm'])
			{
				if(create_confirm($row['id'], $email, $fullname))
				{
					$status = true;
					$signup['s1'] = true;
				}
			}
			elseif(!$site['join-aConfirm'])
			{
				$status = true;
				$signup['s1'] = true;
			}
		}
	}
	return $status;
}

function startSession($time, $name)
{
    session_set_cookie_params($time);
    session_name($name);
    session_start();

    // Reset the expiration time upon page load
    if (isset($_COOKIE[$name]))
	{
    	setcookie($name, $_COOKIE[$name], time() + $time, "/");
	}
}

function start_uControl()
{
	global $site, $pageStartTime;
	
	//We do need to run certain items right away...
	loadSettings();
	ip_ban();
	
	startSession(3600, 'vPortal_ses');
	
	if((isset($_SESSION['user-auth'])) && (login_review()))
	{
		$site['user']['login-status'] = true;
		
		//Load User Information
		foreach($_SESSION as $key => $value)
		{
			$k = explode('-', $key);
			$tp1 = $k[0];
			$tp2 = $k[1];
			$site[$tp1][$tp2] = $value;
		}
	}
	else
	{
		$site['user']['login-status'] = false;
	}
	
	$pageStartTime = microtime();
	$pageStartArray = explode(" ", $pageStartTime);
	$pageStartTime = $pageStartArray[1] + $pageStartArray[0];
}

function myif($q1, $q2)
{
	if($q1 == $q2)
	{
		return true;
	}
	return false;
}

function box_t($title, $width=412)
{
	global $site;
	
	echo('<table align="center" border="0" cellpadding="0" cellspacing="0" width="' . $width . '">
	<tbody>
		<tr>
			<td background="' . $site['url_path'] . '/images/template/corner_title1.gif" height="20" width="6"></td>
			<td bgcolor="#3b4d61" height="20" width="400"><font color="#ececec"><b>' . $title . '</b></font></td>
			<td background="' . $site['url_path'] . '/images/template/corner_title2.gif" height="20" width="6"></td>
		</tr>
		<tr>
            <td height="2" width="6"></td>
            <td height="2"></td>
            <td height="2" width="6"></td>
        </tr>
        <tr>
			<td bgcolor="#ececec" height="3" width="6"></td>
            <td bgcolor="#ececec" height="3"></td>
            <td bgcolor="#ececec" height="3" width="6"></td>
		</tr>
		<tr>
			<td bgcolor="#ececec" width="6"></td>
			<td bgcolor="#ececec">');
}

function box_b()
{
	global $site;
	
	echo('</td>
			<td bgcolor="#ececec" width="6"></td>
		</tr>
		<tr>
			<td background="' . $site['url_path'] . '/images/template/bottom_left.gif" height="16" width="6"></td>
			<td bgcolor="#ececec" height="16"></td>
        	<td background="' . $site['url_path'] . '/images/template/bottom_right.gif" height="16" width="6"></td>
		</tr>
	</tbody>
</table>');
}

function goodDay()
{	//Good Day System!
	global $site;
	
	if(date('H') < 12){$timeOfDay = 'morning';}elseif(date('H') < 16){$timeOfDay = 'afternoon';}else{$timeOfDay = 'evening';}
	$name = explode(' ', $site['user']['fullname']);
	
	return '<div class="sidebar round" id="oBox-goodDay"><div class="sidebar round" id="iBox-goodDay"><b>Good ' . $timeOfDay . ' ' . $name[0] . '!</b><br/><br />Date: ' . date('m/d/Y') . '</div></div>';
	
	//return 'Good' . $timeOfDay . ' ' . $name[0] . '!<br/><br />Time: ' . date('h:i:s a') . '<br />Date: ' . date('m/d/Y') . '<br /><br /> Sunrise: ' . date_sunrise(time(), SUNFUNCS_RET_STRING, 38.4, -9, 90, -5) . ' am' . '<br />Sunset: ' . date_sunset(time(), SUNFUNCS_RET_STRING, 38.4, -9, 90, -5) . ' pm<br />';
}

function sidebarInclude($text = '', $action = 'post')
{
	global $site;
	
	if($action == 'get')
	{
		echo($site['sidebarInclude']);
	}
	else
	{
		$site['sidebarInclude'] .= $text;
	}
}

function sideBar()
{
	global $site;
	
	?>
    	</div>
        <div class="body" id="sidebar">
        	<?php sidebarInclude('', 'get'); ?>
        	<br />
        	<center><img src="<?php echo($site['url_path']); ?>/images/designs/ad.png" border="1"></center>
        </div>
    	<div class="dummy"></div>
	</div>
	</center>
    <?php
}

function footer_b()
{
	global $site, $sideBar;
	
	if(isset($sideBar)){ sideBar(); }
	echo('<br /><br /><center>
		<div class="footer round" id="oBox">
    		<div class="footer" id="iBox">
			<a href="' . $site['url_path'] . '/info.php" alt="Information" title="Information">vPortal</a> &copy; ' . date('Y'));
	if($site['user']['login-status'])
	{
		echo(' | <a href="' . $site['url_path'] . '/home.php" alt="Home">Home</a>');
		if($site['user']['permissions'][0][6]){echo(' | <a href="' . $site['url_path'] . '/members.php" alt="Members">Members</a>');}
	}
	echo(' | <a href="' . $site['url_path'] . '/info.php" alt="Information" title="Information">Information</a>');
	echo('</div>
   	 	</div></center>
		</body></html>');
		
	$pageEndTime = microtime();
	$pageEndArray = explode(" ", $pageEndTime);
	$pageEndTime = $pageEndArray[1] + $pageEndArray[0];
	$pageTotalTime = $pageEndTime - $pageStartTime; 
	$pageTotalTime2 = round($pageTotalTime,5);
	echo('<!-- All Served Up! ' . $pageTotalTime . ' - ' . $pageTotalTime2 . ' -->');
}

function logged_in($type=NULL)
{
	global $site;
	
	if(!$site['user']['login-status'])
	{
		header('location: ' . $site['url_path'] . '/login.php');
		exit;
	}
	elseif($type == 'index')
	{
		header('location: ' . $site['url_path'] . '/home.php');
		exit;
	}
	elseif(($type == 'admin') && ($site['user']['level'] < 2))
	{
		header('location: ' . $site['url_path'] . '/index.php');
		exit;
	}
}

function build_bank_balance()
{	
	global $site, $conn;
	
	//Get balances from profiles? OFF
	/*$rs = mysql_query("SELECT `id`, `balance` FROM users");
	while($row = mysql_fetch_array($rs))
	{
		$balance[$row['id']] = $row['balance'];
	}
	*/
	
	//Go through tans and develop balances...
	$rs = mysql_query("SELECT * FROM baccount", $conn);
	
	if(mysql_num_rows($rs) > 0)
	{
		while($row = mysql_fetch_array($rs))
		{
			if($row['action'] == '+')
			{
				$balance[$row['userId']] = $balance[$row['userId']] + $row['amount'];
			}
			
			if($row['action'] == '-')
			{
				$balance[$row['userId']] = $balance[$row['userId']] - $row['amount'];
			}
			
			if($row['date'] == date('m/d/Y'))
			{
				$mail = mailAway($row2['email'], 'Bank Update', 'Your bank balance has been updated. You can view it here: ' . $site['url_path'] . '/bankAccount.php');
			}
		}
	}
	
	//Store new balance
	foreach($balance as $key => $value)
	{
		$rs = mysql_query("UPDATE users SET `balance` = '" . $value . "' WHERE `id` = '" . $key . "'", $conn);
	}
}

function buld_permissions_old($level=NULL)
{
	global $site, $conn;
	
	if($level == NULL)
	{
		$level = $site['user']['level'];
	}
	
	$rs = mysql_query("SELECT `permissions` FROM levels WHERE `id` = '" . $level . "'", $conn);
	
	if(mysql_num_rows($rs) == 1)
	{
		$row = mysql_fetch_array($rs);
		
		$per = explode('-', $row['permissions']);
		
		$tp1 = sizeof($per)-1;
		
		for($v=0; $v<=$tp1; $v++)
		{
			$tp2 = $per[$v];
			$tp3 = strlen($tp2)-1;
			for($i=0; $i<=$tp3; $i++)
			{
				$x = $i;
				$site['user']['permissions'][$v][$i] = $tp2[$x];
			}
		}
	}
}

function buld_permissions($level=NULL)
{
	global $site, $conn;
	
	if($level == NULL)
	{
		$level = $site['user']['level'];
	}
	
	$rs = mysql_query("SELECT `permissions` FROM levels WHERE `id` = '" . $level . "'", $conn);
	
	if(mysql_num_rows($rs) == 1)
	{
		$row = mysql_fetch_array($rs);
		
		$per = explode('[*]', $row['permissions']);
		
		foreach($per as $key => $value_1)
		{
			$hold_1 = explode(',', $value_1);

			foreach($hold_1 as $value_2)
			{
				$hold_2 = explode('=>', $value_2);
				$site['user']['permissions'][$key][$hold_2['0']] = $hold_2['1'];
			}
		}
	}
}

function page_permissions($area, $section)
{
	global $site, $conn;
	
	if($site['user']['permissions'][$area][$section] != 1)
	{
		if($area==1){$admin_header = true;}
		if($area==0){$file='./designs/header.php';}else{$file='../designs/header.php';}
		include_once($file);
		echo('<br /><br />');
		box_t('Permissions');
		echo('You do not have access to this area.');
		box_b();
		exit;
	}
}

function build_user_session($id)
{
	global $site, $conn;
	
	$rs = mysql_query("SELECT * FROM users WHERE `id` = '" . $id . "'", $conn);
	
	if(mysql_num_rows($rs)==1)
	{
		foreach(mysql_fetch_array($rs) as $key => $value)
		{
			if(!is_int($key))
			{
				$_SESSION['user-' . $key] = $value;
			}
		}
		return true;
	}
	else
	{
		return false;
	}
}

function dobry_den_quotes()
{
	/** These are the Quotes we will see! */
	$quotes = "I keep thinking that this is going to get better.
In time we hate that which we often fear.
I wasted time, and now doth time waste me.
I must be cruel only to be kind; Thus bad begins, and worse remains behind.
We never fail when we try to do our duty, we always fail when we neglect to do it.
A boy carries out suggestions more wholeheartedly when he understands their aim.
In Scouting, a boy is encouraged to educate himself instead of being instructed.
A Scout smiles and whistles under all circumstances.
See things from the boy's point of view.
Trust should be the basis for all our moral training.
A Scout is never taken by surprise; he knows exactly what to do when anything unexpected happens.
O God, help me to win, but in thy wisdom if thou willest me not to win, then O God, make me a good loser.
In all of this, it is the spirit that matters. Our Scout law and Promise, when we really put them into practice, take away all occasion for wars and strife among nations.
The uniform makes for brotherhood, since when universally adopted it covers up all differences of class and country.
We came here as strangers but leave as brothers.
Believe nothing against another but on good authority; and never report what may hurt another, unless it be a greater hurt to some other to conceal it.
How wonderful it is that nobody need wait a single moment before starting to improve the world.";

	// Here we split it into lines
	$quotes = explode("\n", $quotes);

	// And then randomly choose a line
	return $quotes[mt_rand(0, count($quotes)-1)];
}

function cleanString($string, $type = 'text-input')
{
	if($type == 'text-input')
	{
		if(get_magic_quotes_gpc())
		{	//Yes!!
			$string = stripslashes($string);
		}
		$string = mysql_real_escape_string($string);
	}
	elseif($type == 'text-output')
	{
		$string = str_replace('\\', '', $string);
	}
	
	return $string;
}

function ip_ban()
{
	global $site;
	
	$list = explode('|', $site['ip-ban']);
	
	foreach($list as $key => $value)
	{
		if($value == $_SERVER['REMOTE_ADDR'])
		{
			header("HTTP/1.0 404 Not Found");
			exit;
		}
	}
}

function disguise_curl($url) 
{ 
	$curl = curl_init(); 
	// Same headers from Firefox version 2.0.0.6 
	$header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,"; 
	$header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5"; 
	$header[] = "Cache-Control: max-age=0"; 
	$header[] = "Connection: keep-alive"; 
	$header[] = "Keep-Alive: 300"; 
	$header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7"; 
	$header[] = "Accept-Language: en-us,en;q=0.5"; 
	$header[] = "Pragma: "; // browsers keep this blank. 
	curl_setopt($curl, CURLOPT_URL, $url); 
	curl_setopt($curl, CURLOPT_USERAGENT, 'Googlebot/2.1 (+http://www.google.com/bot.html)'); 
	curl_setopt($curl, CURLOPT_HTTPHEADER, $header); 
	curl_setopt($curl, CURLOPT_REFERER, 'http://www.google.com'); 
	curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate'); 
	curl_setopt($curl, CURLOPT_AUTOREFERER, true); 
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($curl, CURLOPT_TIMEOUT, 10); 
	$html = curl_exec($curl);
	curl_close($curl);
	return $html;
} 

function grabFeed($url, $key)
{
	if(function_exists('curl_version'))
	{	//First try curl... if on
		$data = disguise_curl($url . '?k=' . $key);
		
		return $data;
	}
	elseif(ini_get('allow_url_fopen'))
	{	//Try fopen... if on
		if(file_exists($url))
		{
			ini_set('user_agent','MSIE 4\.0b2;');
			$dataFile = fopen($url . '?k=' . $key, "r") or die();
			$data = fread($dataFile, 1000000); 
			fclose($dataFile);
		}
		else
		{
			$data = '';
		}
		
		return $data;
	}
	else
	{	//No global news feed.
		
		return '';
	}
}

function sepFeed($data)
{
	foreach(explode('[*]', $data) as $key => $value)
	{
		list($feed[$key]['title'], $feed[$key]['mType'], $feed[$key]['by'], $feed[$key]['date'], $feed[$key]['content']) = explode('{*}', $value);
	}
	
	return $feed;
}

function startFCK($dir = NULL)
{
	include($dir . "./includes/fckeditor/fckeditor.php");
}

function filePermission($path, $permission)
{
    clearstatcache();
    $configmod = substr(sprintf('%o', fileperms($path)), -4); 
	
    if($configmod != $permission)
	{
		return false;
	}
	else
	{
		return true; 
	}
}
?>