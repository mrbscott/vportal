<html>
	<head>
    	<title><?php echo($site['site_name']); ?> - Powered by vPoral</title>
		<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
        <link href="<?php echo($site['url_path']); ?>/designs/css/style.css" rel="stylesheet" type="text/css">
        <?php
		if(isset($sortTable)){echo('<script src="' . $site['url_path'] . '/designs/js/sorttable.js" type="text/javascript"></script>');}
		if(isset($calButton)){echo('<script src="' . $site['url_path'] . '/designs/js/calendar.js" type="text/javascript"></script>');}
		if(isset($boxy)){echo('<script type="text/javascript" src="' . $site['url_path'] . '/designs/js/jquery.js"></script><script type="text/javascript" src="' . $site['url_path'] . '/designs/js/jquery.boxy.js"></script>');}
		?>
	</head>
    <body>
    	<div class="header" id="header">
            <div class="header" id="logo">
                <a href="<?php echo($site['url_path']); ?>/index.php"><img src="<?php echo($site['url_path']); ?>/images/template/logo.gif" border="0"></a>
            </div>
            <div class="header" id="sign">
                <a href="http://medventrovers.ca"><img src="<?php echo($site['url_path']); ?>/images/template/ad.png" border="0"></a>
            </div>
            <div class="dummy">
            </div>
            <div class="header" id="headline"><?php if(($site['user']['login-status']) && ($site['user']['permissions'][0]['quotes'])){echo(dobry_den_quotes());} ?></div>
        </div>
        <div class="nava" id="nava">
			<?php
			if(!$site['user']['login-status'])
			{
				if($site['login']){echo('<a href="' . $site['url_path']. '/login.php" title="Login" alt="Login">Login</a>');}
				if($site['signup']){echo(' | <a href="' . $site['url_path'] . '/signup.php" title="Join" alt="Join">Join</a>');}
			}
			elseif(isset($admin_header))
			{
				if($site['user']['permissions'][1]['admin-panel']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/index.php" title="Home" alt="Home">Home</a> | ');}
				if($site['user']['permissions'][1]['view-news']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/news.php" title="News" alt="News">News</a> | ');}
                if($site['user']['permissions'][1]['view-events']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/events.php" title="Events" alt="Events">Events</a> | ');}
                if($site['user']['permissions'][1]['view-bank']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/bank.php" title="Bank" alt="Bank">Bank</a> | ');}
				if($site['user']['permissions'][1]['view-badges']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/badges.php" title="Badges" alt="Badges">Badges</a> | ');}
				if($site['user']['permissions'][1]['view-members']){echo(' <a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/members.php" title="Members" alt="Members">Members</a> | ');}
				if($site['user']['permissions'][1]['settings-view']){echo(' <a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/settings.php" title="Settings" alt="Settings">Settings</a> | ');} ?>
				<a href="<?php echo($site['url_path']); ?>/login.php?logout">Logout</a>
                <?php
			}
			else
			{   
				?>
                <a href="<?php echo($site['url_path']); ?>/home.php" title="Home" alt="Home">Home</a> | <?php
                if($site['user']['permissions'][0]['view-profiles']){echo('<a href="' . $site['url_path'] . '/viewProfile.php?id=' . $site['user']['id'] . '" title="My Profile" alt="My Profile">Profile</a> | ');}
              	if($site['user']['permissions'][0]['view-events']){echo('<a href="' . $site['url_path'] . '/events.php" title="Events" alt="Events">Events</a> | ');}
                if($site['user']['permissions'][0]['bank-account']){echo('<a href="' . $site['url_path'] . '/bankAccount.php" title="Bank" alt="Bank">Bank</a> | ');}
				if($site['user']['permissions'][0]['view-mail']){
					echo('<a href="' . $site['url_path'] . '/mail.php">Mail</a>');
					$rs = mysql_query("SELECT COUNT(*) AS `total` FROM mail WHERE `to` = '" . $site['user']['id'] . "' AND `parent` = '' AND `viewed` = ''", $conn);
					if(mysql_result($rs, 0, "total")>0)
					{
						echo(' <a href="' . $site['url_path'] . '/mail.php" id="inbox" class="round nava">&nbsp;' . mysql_result($rs, 0, "total") . '&nbsp;</a>');
					}
					echo(' | ');
				}
				if($site['user']['permissions'][0]['view-badges']){echo('<a href="' . $site['url_path'] . '/badges.php" title="Badges" alt="Badges">Badges</a> | ');}
				if($site['user']['permissions'][0]['view-forums']){echo('<a href="' . $site['url_path'] . '/forums.php" title="Forums" alt="Forums">Forums</a> | ');} ?>
                <a href="<?php echo($site['url_path']); ?>/login.php?logout" title="Logout" alt="Logout">Logout</a>
                <?php
            }
					
			if(($site['user']['permissions'][1]['admin-panel']) && (!isset($admin_header)))
			{
				?>    
                | <a href="<?php echo($site['url_path'] . '/' . $site['admin_path']); ?>/index.php" class="admin" id="admin-link" title="Admin Panel" alt="Admin Panel">Admin</a>
               	<?php
			}
			elseif(($site['user']['permissions'][0]['login']) && (isset($admin_header)))
			{
				?>    
                | <a href="<?php echo($site['url_path']); ?>/index.php" class="admin" id="admin-link" title="Home" alt="Home">Home</a>
               	<?php
			}
			?>
		</div>
        <br />
        <?php
		if(($site['user']['login-status']) && (!isset($admin_header)) && (!isset($sideBarOff))){$sideBar=true;}
		
		if(isset($sideBar))
		{
			?>
            <center>
            <div class="body" id="bBox">
                <div class="body" id="content">
            <?php
		}
		?>