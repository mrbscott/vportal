<?php
include_once('./includes/settings.inc.php');
include_once('./includes/connection.php');
include_once('./includes/functions.inc.php');

start_uControl();
if(isset($_POST['join']))
{
	$site['status']['signup'] = signup();
}

if(isset($_GET['confirm']))
{
	$site['status']['confirm'] = email_confirm(mysql_real_escape_string($_GET['confirm']));
}

include_once('./designs/header.php');
?>
<br />
<br />
<?php
box_t('Join');

if(isset($signup))
{
	if(isset($signup['s1']))
	{	//Already has an account
		echo('You have now been registered in the system.');
		echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/login.php" /> ');
	}
	if(isset($signup['s4']))
	{	//Email already in use.
		echo('Email already in use. Please <a href="javascript:history.go(-1)">click here</a> to go back.');
	}
	if(isset($signup['s3']))
	{	//Username taken!
		echo('Please choose another username. The one chosen is currently in use. Please <a href="javascript:history.go(-1)">click here</a> to go back.');
	}
	if(isset($signup['s2']))
	{	//Not everything has been filled in.
		echo('Please make sure that all information is filled in. Please <a href="javascript:history.go(-1)">click here</a> to go back.');
	}
}
elseif((isset($_GET['confirm'])) && ($site['status']['confirm']))
{
	echo('Your account is now active.');
	echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/login.php" /> ');
}
elseif(!$site['signup'])
{
	echo('vPanel signup is currently turned off.');
	echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/index.php" /> ');
}
else
{	//Main Page
	?>
	<form name="s-signup" method="post" action="<?php echo($site['url_path']); ?>/signup.php">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  		<tr>
        	<td colspan="2"><b>Join</b></td>
        </tr>
        <tr>
        	<td colspan="2">&nbsp;</td>
        </tr>
        <tr>
    		<td>Username:</td>
            <td><input type="text" name="username" id="username" /></td>
        </tr>
        <tr>
        	<td>Password:</td>
            <td><input type="password" name="password" id="password" /></td>
        </tr>
        <tr>
        	<td>Email:</td>
            <td><input type="text" name="email" id="email" /></td>
        </tr>
        <tr>
        	<td>Full Name:</td>
			<td><input type="text" name="fullname" id="fullname" /></td>
        </tr>
        <tr>
        	<td>First Aid:</td>
			<td>
            	<select name="firstaid" id="firstaid">
				<?php
				$rs = mysql_query("SELECT * FROM firstaid WHERE `status` = '1' ORDER BY `id` ASC", $conn);
				while($row = mysql_fetch_array($rs))
				{
					echo('<option value="' . $row['id'] . '">' . $row['name'] . '</option>');
				}
            	?>
                </select>
            </td>
        </tr>
        <tr>
        	<td>Membership Type:</td>
			<td>
            	<select name="mType" id="mType">
				<?php
				$rs = mysql_query("SELECT * FROM mTypes WHERE `status` = '1' ORDER BY `id` ASC", $conn);
				while($row = mysql_fetch_array($rs))
				{
					echo('<option value="' . $row['id'] . '">' . $row['name'] . '</option>');
				}
            	?>
                </select>
            </td>
        </tr>
        <tr>
        	<td colspan="2"><input type="submit" name="join" id="join" value="Register" /></td>
        </tr>
    </table>
	</form>
	<?php
}
box_b();
footer_b();
?>