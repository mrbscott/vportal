<?php
include_once('./includes/settings.inc.php');
include_once('./includes/connection.php');
include_once('./includes/functions.inc.php');
start_uControl();
logged_in();
buld_permissions();
page_permissions(0, 'view-mail');
include_once('./designs/header.php');

if($site['mail'])
{	//Are we online?
if(isset($_GET['remove']))
{
	$rs = mysql_query("", $conn);
}
elseif(isset($_POST['send-c']))
{	//Pre and send new msg
	if(($_POST['to'] == NULL) || ($_POST['important'] == NULL) || ($_POST['subject'] == NULL) || ($_POST['content'] == NULL))
	{
		$eCheck = true;
	}
	
	if(!isset($eCheck))
	{
		$rs = mysql_query("INSERT INTO msg (`subject`, `content`) VALUES ('" . mysql_real_escape_string($_POST['subject']) . "', '" . mysql_real_escape_string($_POST['content']) . "')", $conn);
		
		$rs = mysql_query("SELECT `id` FROM msg WHERE `content` = '" . mysql_real_escape_string($_POST['content']) . "'", $conn);
		$row = mysql_fetch_array($rs);
		
		$rs = mysql_query("INSERT INTO mail (`to`, `from`, `cId`, `important`) VALUES ('" . mysql_real_escape_string($_POST['to']) . "', '" . $site['user']['id'] . "', '" . $row['id'] . "', '" . mysql_real_escape_string($_POST['important']) . "')", $conn);
		
		$rs2 = mysql_query("SELECT `email` FROM users WHERE `id` = '" . mysql_real_escape_string($_POST['to']) . "'", $conn);
		$row2 = mysql_fetch_array($rs2);
		
		$mail = mailAway($row2['email'], 'New Message', 'You have a new message in your inbox from ' . $site['user']['fullname'] . '. You can view it here; ' . $site['url_path'] . '/mail.php');
		echo('<br />');
		box_t('Mail');
		echo('Your message has been sent.');
		echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/members.php" />');
		box_b();
	}
}
elseif(isset($_POST['send-r']))
{	//Replys
	if(($_POST['subject'] == NULL) || ($_POST['content'] == NULL))
	{
		$eCheck = true;
	}
	
	if(!isset($eCheck))
	{
		$rs = mysql_query("INSERT INTO msg (`subject`, `content`) VALUES ('" . mysql_real_escape_string($_POST['subject']) . "', '" . mysql_real_escape_string($_POST['content']) . "')", $conn);
		
		$rs = mysql_query("SELECT `id` FROM msg WHERE `content` = '" . mysql_real_escape_string($_POST['content']) . "'", $conn);
		$row = mysql_fetch_array($rs);
		
		$rs2 = mysql_query("SELECT * FROM mail WHERE `id` = '" . mysql_real_escape_string($_GET['view']) . "'");
		$row2 = mysql_fetch_array($rs2);
		
		if($site['user']['id']==$row2['to']){$to=$row2['to'];}else{$to=$row2['from'];}
		
		$rs = mysql_query("INSERT INTO mail (`to`, `from`, `cId`, `parent`) VALUES ('" . $to. "', '" . $site['user']['id'] . "', '" . $row['id'] . "', '" . $row2['id'] . "')", $conn);
		
		$rs3 = mysql_query("SELECT `email` FROM users WHERE `id` = '" . mysql_real_escape_string($_POST['to']) . "'", $conn);
		$row3 = mysql_fetch_array($rs3);
		
		$mail = mailAway($row3['email'], 'New Message', 'You have a new message in your inbox from ' . $site['user']['fullname'] . '. You can view it here; ' . $site['url_path'] . '/mail.php');
	}
}

?>
<br />
<table align="center" width="75%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    	<td width="25%">
        	<?php if($site['user']['permissions'][0]['view-mail']){echo('<a href="' . $site['url_path'] . '/mail.php" class="mail" id="inbox-message">Inbox</a>');} ?>
        </td>
        <td width="25%">
        	<?php if($site['user']['permissions'][0]['view-sent']){echo('<a href="' . $site['url_path'] . '/mail.php?sent" class="mail" id="sent-message">Sent</a>');} ?>
        </td>
    	<td width="25%">&nbsp;
        	
        </td>
    	<td width="25%">
        	<?php if($site['user']['permissions'][0]['create-mail']){echo('<a href="' . $site['url_path'] . '/mail.php?create" class="mail" id="new-message"><img src="' . $site['url_path'] . '/images/icons/add.gif" border="0"> New Message</a>');} ?>
        </td>
	</tr>
</table>
<br />
<br />
<?php
if(isset($_GET['create']))
{	//Create Mail
	?>
	<table align="center" width="45%" border="0" cellspacing="0" cellpadding="0">
    	<form method="post" action="<?php echo($site['url_path']); ?>/mail.php" name="u-Addmail" id="u-Addmail">
    	<tr>
        	<td colspan="2"><b>Create Message</b></td>
        </tr>
        <tr>
        	<td colspan="2">&nbsp;</td>
        </tr>
        <tr>
        	<td>To:</td>
            <td>
				<select name="to" id="to">
                	<?php
					$rs3 = mysql_query("SELECT `id`, `fullname` FROM users ORDER BY `fullname` ASC", $conn);
                    while($row3 = mysql_fetch_array($rs3))
					{
						echo('<option value="' . $row3['id'] . '"');
						if((isset($_GET['to'])) && ($_GET['to'] == $row3['id'])){echo(' selected');}
						echo('>' . $row3['fullname'] . '</option>');
					}
					?>
                </select>
            </td>
        </tr>
        <tr>
        	<td>From:</td>
            <td><input name="from" type="text" id="from" value="<?php echo($site['user']['fullname']); ?>" readonly="true" /></td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
        	<td>Subject:</td>
            <td><input type="text" name="subject" id="subject" /></td>
        </tr>
        <tr>
        	<td>Importance:</td>
            <td>
                <select name="important" id="important">
                	<option value="0">No Rush</option>
                    <option value="1">Hurry Up</option>
                </select>
            </td>
        </tr>
        <tr>
        	<td>Message:</td>
            <td><textarea name="content" cols="45" rows="5" id="content"></textarea></td>
        </tr>
        <tr>
        	<td><input type="submit" name="send-c" id="send-c" value="Send" /></td>
            <td>&nbsp;</td>
        </tr>
        </form>
    </table>
    <?php
}
elseif(isset($_GET['view']))
{	//View Mail
	$rs = mysql_query("SELECT * FROM mail WHERE `id` = '" . mysql_real_escape_string($_GET['view']) . "'", $conn);
	
	if(mysql_num_rows($rs))
	{
		$row = mysql_fetch_array($rs);
		
		if($row['viewed'] == 0)
		{
			$rs6 = mysql_query("UPDATE mail SET `viewed` = '1' WHERE `id` = '" . $row['id'] . "'", $conn);
		}
		
		$rs2 = mysql_query("SELECT * FROM msg WHERE `id` = '" . $row['cId'] . "'", $conn);
		$row2 = mysql_fetch_array($rs2);
		
		$rs3 = mysql_query("SELECT `fullname` FROM users WHERE `id` = '" . $row['from'] . "'", $conn);
		$row3 = mysql_fetch_array($rs3);

		?>
        <center>
        <div class="mail" id="oBox-view">
        	<div class="mail" id="iBox-view">
            	<div class="mail" id="title-view"><?php echo($row2['subject']); ?></div>
                <div class="mail" id="from-date-view"><?php echo($row3['fullname'] . '<br />' . substr($row['date'], 0, 10)); ?></div>
                <div class="mail" id="content-view"><?php echo($row2['content']); ?></div>
                <div class="dummy"></div>
            </div>
        </div>
       	</center>
        <br />
        <?php
		$rs4 = mysql_query("SELECT * FROM mail WHERE `parent` = '" . $row['id'] . "'", $conn);
		
		if(mysql_num_rows($rs4))
		{	
			while($row4 = mysql_fetch_array($rs4))
			{
				if($row['viewed'] == 0)
				{
					$rs7 = mysql_query("UPDATE mail SET `viewed` = '1' WHERE `id` = '" . $row4['id'] . "'", $conn);
				}
				
				$rs5 = mysql_query("SELECT * FROM msg WHERE `id` = '" . $row4['cId'] . "'", $conn);
				$row5 = mysql_fetch_array($rs5);
				
				$rs6 = mysql_query("SELECT `fullname` FROM users WHERE `id` = '" . $row4['from'] . "'", $conn);
				$row6 = mysql_fetch_array($rs6);
				?>
                <center>
                <div class="mail" id="oBox-view">
                    <div class="mail" id="iBox-view">
                        <div class="mail" id="title-view"><?php echo($row5['subject']); ?></div>
                        <div class="mail" id="from-date-view"><?php echo($row6['fullname'] . '<br />' . substr($row4['date'], 0, 10)); ?></div>
                        <div class="mail" id="content-view"><?php echo($row5['content']); ?></div>
                        <div class="dummy"></div>
                    </div>
                </div>
                </center>
                <br />
                <?php
			}
		}
		
		if($site['user']['permissions'][0]['create-mail'])
		{	//Can Reply
			?>
            <br />
            <br />
            <table align="center" width="45%" border="0" cellspacing="0" cellpadding="0">
                <form method="post" action="<?php echo($site['url_path']); ?>/mail.php?view=<?php echo($row['id']); ?>" name="u-Replymail" id="u-Replymail">
                <tr>
                    <td>Subject:</td>
                    <td><input type="text" name="subject" id="subject" value="RE: <?php echo($row2['subject']); ?>" /></td>
                </tr>
                <tr>
                    <td>Message:</td>
                    <td><textarea name="content" cols="45" rows="5" id="content"></textarea></td>
                </tr>
                <tr>
                    <td><input type="submit" name="send-r" id="send-r" value="Relpy" /></td>
                    <td>&nbsp;</td>
                </tr>
                </form>
    		</table>
            <?php
		}
	}
	else
	{
		//Not found
		box_t('Mail');
		echo('Mail could not be found.');
		echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/mail.php" /> ');
		box_b();
	}
}
elseif(isset($_GET['sent']))
{	//Sent Mailbox
	$rs = mysql_query("SELECT * FROM mail WHERE `from` = '" . $site['user']['id'] . "' ORDER BY `id` DESC", $conn);
	
	if(mysql_num_rows($rs))
	{
		while($row = mysql_fetch_array($rs))
		{
			$rs2 = mysql_query("SELECT * FROM msg WHERE `id` = '" . $row['cId'] . "'", $conn);
			$row2 = mysql_fetch_array($rs2);
			
			$rs3 = mysql_query("SELECT * FROM users WHERE `id` = '" . $row['from'] . "'", $conn);
			$row3 = mysql_fetch_array($rs3);
			?>
            <div class="mail" id="oBox-inbox">
            	<div class="mail" id="iBox-inbox">
                	<div class="mail" id="title-inbox">
                    	<a href="<?php echo($site['url_path'] . '/mail.php?view=' . $row['id']); ?>" alt="<?php echo($row2['subject']); ?>" title="<?php echo($row2['subject']); ?>"><?php echo($row2['subject']); ?></a>
                    </div>
 					<div class="mail" id="from-sent">From: <?php echo($row3['fullname']); ?></div>
                    <div class="mail" id="date-sent">Date: <?php echo(substr($row['date'], 0, 10)); ?></div>
                    <div class="dummy"></div>
                    <div class="mail" id="content-sent">Message: <?php echo(substr($row2['content'], 0, 55)); if(strlen($row2['content'])>55)echo('...'); ?></div>
            	</div>
                <br />
                <?php $x++; if($x < mysql_num_rows($rs)){echo('<hr />');} ?>
            </div>
            <?php
		}
	}
	else
	{
		//Not found
		box_t('Mail');
		echo('Mailbox empty.');
		box_b();
	}
}
else
{	//Inbox
	$rs = mysql_query("SELECT * FROM mail WHERE `to` = '" . $site['user']['id'] . "' ORDER BY `id` DESC", $conn);
	
	if(mysql_num_rows($rs))
	{
		echo('<center>');
		while($row = mysql_fetch_array($rs))
		{
			$rs2 = mysql_query("SELECT * FROM msg WHERE `id` = '" . $row['cId'] . "'", $conn);
			$row2 = mysql_fetch_array($rs2);
			
			$rs3 = mysql_query("SELECT * FROM users WHERE `id` = '" . $row['from'] . "'", $conn);
			$row3 = mysql_fetch_array($rs3);
			?>
            <div class="mail" id="oBox-inbox">
            	<div class="mail" id="iBox-inbox">
                	<div class="mail" id="title-inbox">
                    	<a href="<?php echo($site['url_path'] . '/mail.php?view=' . $row['id'] . '" alt="' . $row2['subject'] . '" title="' . $row2['subject'] . '">' . $row2['subject']); ?></a><?php if($row['viewed'] == 0){echo(' <font class="mail" id="unread-message-inbox">new</font>');} ?><?php if($row['important'] == 1){echo(' <font class="mail" id="important-message-inbox">Read Me!</font>');}?>
                    </div>
                    <div class="mail" id="from-inbox">From: <?php echo($row3['fullname']); ?></div>
                    <div class="mail" id="date-inbox">Date: <?php echo(substr($row['date'], 0, 10)); ?></div>
                    <div class="dummy"></div>
                    <div class="mail" id="content-inbox">Message: <?php echo(substr($row2['content'], 0, 55)); if(strlen($row2['content'])>55)echo('...'); ?></div>
            	</div>
                <br />
                <?php $x++; if($x < mysql_num_rows($rs)){echo('<hr />');} ?>
            </div>
        	<?php
		}
	}
	else
	{
		//Not found
		box_t('Mail');
		echo('Mailbox empty.');
		box_b();
	}
}
}
else
{	//Events is offline...
	echo('<br /><br />');
	box_t('Mail');
	?>
    The mail area is currently offline.
    <?php
	box_b();
}
footer_b();
?>