<?php
include_once('../includes/settings.inc.php');
include_once('../includes/connection.php');
include_once('../includes/functions.inc.php');
start_uControl();
logged_in('admin');
buld_permissions();
page_permissions(1, 'view-news');
$admin_header = true;
$sortTable = true;
include_once('../designs/header.php');

if(isset($_GET['remove']))
{
	$rs = mysql_query("DELETE FROM news WHERE `id` = '" . mysql_real_escape_string($_GET['remove']) . "'", $conn);
}
elseif(isset($_POST['create']))
{
	if(($_POST['title'] == NULL) || ($_POST['content'] == NULL))
	{
		$eCheck = true;
	}
	
	if(!isset($eCheck))
	{
		$sql = "INSERT INTO news (`title`, `by`, `to`, `status`, `content`) VALUES 
									('" . mysql_real_escape_string($_POST['title']) . "', '" . mysql_real_escape_string($_POST['by']) . "', '" . mysql_real_escape_string($_POST['to']) . "', '" . mysql_real_escape_string($_POST['status']) . "', '" . mysql_real_escape_string($_POST['content']) . "')";
		
		$rs = mysql_query($sql, $conn);
		echo('<br /><br />');
		box_t('News');
		echo('Post has been added.');
		echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/news.php" /> ');
		box_b();
	}
	else
	{
		echo('<br /><br />');
		box_t('News');
		echo('Post could not be added.');
		box_b();
	}
}
elseif(isset($_POST['change']))
{
	if(($_POST['title'] == NULL) || ($_POST['content'] == NULL))
	{
		$eCheck = true;
	}
	
	if(!isset($eCheck))
	{
		$sql = "UPDATE news SET `title` = '" . mysql_real_escape_string($_POST['title']) . "', `by` = '" . mysql_real_escape_string($_POST['by']) . "', `to` = '" . mysql_real_escape_string($_POST['to']) . "', `status` = '" . mysql_real_escape_string($_POST['status']) . "', `content` = '" . mysql_real_escape_string($_POST['content']) . "' WHERE `id` = '" . mysql_real_escape_string($_POST['id']) . "'";
		$rs = mysql_query($sql, $conn);
		echo('<br /><br />');
		box_t('News');
		echo('Post has been updated.');
		echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/news.php" /> ');
		box_b();
	}
	else
	{
		echo('<br /><br />');
		box_t('News');
		echo('Post could not be updated.');
		box_b();
	}
}
?>
<br />
<br />
<table align="center" width="75%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    	<td>
        	<?php if($site['user']['permissions'][1]['add-news']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/news.php?add">Add Post</a>');} ?>
        </td>
    	<td>&nbsp;</td>
    	<td>&nbsp;</td>
    	<td>&nbsp;</td>
	</tr>
</table>
<br />
<?php
if((!isset($_GET['add'])) && (!isset($_GET['edit'])))
{	//Main Page
	$rs = mysql_query("SELECT * FROM news", $conn);
	
	?>
    <table align="center" class="sortable" width="75%" border="0" cellspacing="0" cellpadding="0">
  		<tr>
    		<td><b>ID</b></td>
    		<td><b>Title</b></td>
   			<td><b>Date</b></td>
    		<td><b>By</b></td>
            <td><b>To</b></td>
            <td><b>Status</b></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
  		</tr>
    	<?php
		while($row = mysql_fetch_array($rs))
		{
			$rs2 = mysql_query("SELECT `username` FROM users WHERE `id` = '" . $row['by'] . "'", $conn);
			$row2 = mysql_fetch_array($rs2);
			
			if($row['to'] > 0)
			{
				$rs3 = mysql_query("SELECT `name` FROM levels WHERE `id` = '" . $row['to'] . "'", $conn);
				$row3 = mysql_fetch_array($rs3);
			}
			else
			{
				$row3['name'] = 'Everyone';
			}
		?>
  		<tr>
    		<td><?php echo($row['id']); ?></td>
    		<td><?php echo($row['title']); ?></td>
    		<td><?php echo(substr($row['created'], 0, 10)); ?></td>
    		<td><?php echo($row2['username']); ?></td>
            <td><?php echo($row3['name']); ?></td>
            <td><?php if($row['status'] == 1){echo('Live');}else{echo('Draft');} ?></td>
            <td>
            	<?php if($site['user']['permissions'][1]['edit-news']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/news.php?edit=' . $row['id'] . '"><img src="' . $site['url_path'] . '/images/icons/edit.gif" title="Edit" alt="Edit" border="0"></a>');} ?>
            </td>
            <td>
            	<?php if($site['user']['permissions'][1]['remove-news']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/news.php?remove=' . $row['id'] . '"><img src="' . $site['url_path'] . '/images/icons/delete.gif" title="Remove" alt="Remove" border="0"></a>');} ?>
            </td>
  		</tr>
        <?php
	}
	echo('</table>');
}
elseif(isset($_GET['add']))
{	//Add Post
	?>
    <form action="<?php echo($site['url_path'] . '/' . $site['admin_path'] . '/news.php'); ?>" method="post" name="a-Addpost">
    <table align="center" width="30%" border="0" cellspacing="0" cellpadding="0">
  		<tr>
        	<td colspan="2"><b>Add Post</b></td>
        </tr>
        <tr>
        	<td colspan="2">&nbsp;</td>
        </tr>
        <tr>
    		<td>Title:</td>
        	<td><input type="text" name="title" id="title" /></td>
        </tr>
        <tr>
        	<td>By:</td>
        	<td>
            <select name="by" id="by">
				<?php
				$rs = mysql_query("SELECT * FROM users WHERE `level` > '0'", $conn);
				while($row = mysql_fetch_array($rs))
				{
					echo('<option value="' . $row['id'] . '"');
					if($row['id'] == $site['user']['id']){ echo(' selected'); }
					echo('>' . $row['username'] . '</option>');
				}
            	?>
            </select>
        	</td>
        </tr>
        <tr>
        	<td>To:</td>
        	<td>
            <select name="to" id="to">
            	<option value="0" selected>Everyone</option>
				<?php
				$rs = mysql_query("SELECT * FROM levels WHERE `status` > '0' AND `id` != '0'", $conn);
				while($row = mysql_fetch_array($rs))
				{
					echo('<option value="' . $row['id'] . '">' . $row['name'] . '\'s</option>');
				}
            	?>
        	</select>
            </td>
       	</tr>
        <tr>
        	<td>Status:</td>
        	<td>
            <select name="status" id="status">
				<option value="0">Draft</option>
				<option value="1" selected>Live</option>
        	</select>
            </td>
        </tr>
        <tr>
        	<td>Content:</td>
        	<td><textarea name="content" cols="45" rows="5" id="content"></textarea></td>
        </tr>
        <tr>
        	<td><input type="submit" name="create" id="create" value="Create"></td>
            <td>&nbsp;</td>
        </tr>
    </table>
    </form>
    <?php
}
elseif(isset($_GET['edit']))
{	//Edit Post
	$rs = mysql_query("SELECT * FROM news WHERE `id` = '" . mysql_real_escape_string($_GET['edit']) . "'", $conn);
	
	if(mysql_num_rows($rs) == 1)
	{
		$row = mysql_fetch_array($rs);
		?>
        <form action="<?php echo($site['url_path'] . '/' . $site['admin_path'] . '/news.php'); ?>" method="post" name="a-Editpost">
        <table align="center" width="30%" border="0" cellspacing="0" cellpadding="0">
            <tr>
        		<td colspan="2"><b>Edit Post</b></td>
        	</tr>
        	<tr>
        		<td colspan="2">&nbsp;</td>
        	</tr>
        	<tr>
                <td>Title:</td>
                <td><input type="text" name="title" id="title" value="<?php echo($row['title']); ?>" />
                	<input type="hidden" name="id" id="id" value="<?php echo($row['id']); ?>" />
                </td>
            </tr>
            <tr>
                <td>By:</td>
                <td>
                <select name="by" id="by">
                    <?php
                    $rs2 = mysql_query("SELECT * FROM users WHERE `level` > '0'", $conn);
                    while($row2 = mysql_fetch_array($rs2))
                    {
                        echo('<option value="' . $row2['id'] . '"');
                        if($row2['id'] == $row['by']){ echo(' selected'); }
                        echo('>' . $row2['username'] . '</option>');
                    }
                    ?>
                </select>
                </td>
            </tr>
            <tr>
                <td>To:</td>
                <td>
                <select name="to" id="to">
                    <?php
					if($row['to'] == 0){echo('<option value="0" selected>Everyone</option>');}else{echo('<option value="0">Everyone</option>');}
                    $rs3 = mysql_query("SELECT * FROM levels WHERE `status` > '0' AND `id` != '0'", $conn);
                    while($row3 = mysql_fetch_array($rs3))
                    {
                        echo('<option value="' . $row3['id'] . '"');
						if($row3['id'] == $row['to']){ echo(' selected'); }
						echo('>' . $row3['name'] . '\'s</option>');
                    }
                    ?>
                </select>
                </td>
            </tr>
            <tr>
                <td>Status:</td>
                <td>
                <select name="status" id="status">
                    <option value="0"<?php if($row['status']==0){echo(' selected');} ?>>Draft</option>
                    <option value="1"<?php if($row['status']==1){echo(' selected');} ?>>Live</option>
                </select>
                </td>
            </tr>
            <tr>
                <td>Content:</td>
                <td><textarea name="content" cols="45" rows="5" id="content"><?php echo($row['content']); ?></textarea></td>
            </tr>
            <tr>
                <td><input type="submit" name="change" id="change" value="Change"></td>
                <td>&nbsp;</td>
            </tr>
        </table>
        </form>
    	<?php
	}
	else
	{
		box_t('News');
		echo('No post found.');
		echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/news.php" /> ');
		box_b();
	}
}
echo('</center>');
footer_b();
?>