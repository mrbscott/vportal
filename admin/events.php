<?php
include_once('../includes/settings.inc.php');
include_once('../includes/connection.php');
include_once('../includes/functions.inc.php');
start_uControl();
logged_in('admin');
buld_permissions();
page_permissions(1, 'view-events');
$admin_header = true;
$sortTable = true;
include_once('../designs/header.php');

if((isset($_GET['remove'])) && (isset($_GET['event'])))
{
	$rs = mysql_query("DELETE FROM events WHERE `id` = '" . mysql_real_escape_string($_GET['remove']) . "'", $conn);
	$rs = mysql_query("DELETE FROM evnPar WHERE `eventId` = '" . mysql_real_escape_string($_GET['remove']) . "'", $conn);
}
elseif((isset($_GET['remove'])) && (isset($_GET['category'])))
{
	$rs = mysql_query("DELETE FROM evnType WHERE `id` = '" . mysql_real_escape_string($_GET['remove']) . "'", $conn);
	$_GET['categorys'] = true;
}
elseif(isset($_POST['create-l']))
{
	if($_POST['name'] == NULL)
	{
		$eCheck = true;
	}
	
	if(!isset($eCheck))
	{	
		$sql = "INSERT INTO evnType (`name`, `status`) VALUES ('" . mysql_real_escape_string($_POST['name']) . "', '" . mysql_real_escape_string($_POST['status']) . "')";
		
		$rs = mysql_query($sql, $conn);
		echo('<br /><br />');
		box_t('Events');
		echo('Category has been added.');
		echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/events.php?categorys" /> ');
		box_b();
	}
	else
	{
		echo('<br /><br />');
		box_t('Events');
		echo('Category could not be added.');
		box_b();
	}
}
elseif(isset($_POST['change-e']))
{
	if($_POST['name'] == NULL)
	{
		$eCheck = true;
	}
	
	if(!isset($eCheck))
	{
		$sql = "UPDATE events SET `name` = '" . mysql_real_escape_string($_POST['name']) . "', `type` = '" . mysql_real_escape_string($_POST['type']) . "', `startDate` = '" . mysql_real_escape_string($_POST['startDate']) . "', `endDate` = '" . mysql_real_escape_string($_POST['endDate']) . "', `details` = '" . mysql_real_escape_string($_POST['details']) . "', `org` = '" . mysql_real_escape_string($_POST['org']) . "' WHERE `id` = '" . mysql_real_escape_string($_POST['id']) . "'"; 
		
		$rs = mysql_query($sql, $conn);
		echo('<br /><br />');
		box_t('Events');
		echo('Event has been updated.');
		echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/events.php" /> ');
		box_b();
	}
	else
	{
		echo('<br /><br />');
		box_t('Events');
		echo('Event could not be updated.');
		box_b();
	}
}
elseif(isset($_POST['change-c']))
{
	if($_POST['name'] == NULL)
	{
		$eCheck = true;
	}
	
	if(!isset($eCheck))
	{	
		$rs = mysql_query("UPDATE levels SET `name` = '" . mysql_real_escape_string($_POST['name']) . "', `status` = '" . mysql_real_escape_string($_POST['status']) . "' WHERE `id` = '" . mysql_real_escape_string($_POST['id']) . "'", $conn);
		
		echo('<br /><br />');
		$_GET['levels'] = true;
		box_t('Events');
		echo('Category has been changed.');
		echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/events.php?categorys" /> ');
		box_b();
	}
	else
	{
		echo('<br /><br />');
		box_t('Events');
		echo('Category could not be changed.');
		box_b();
	}
}
?>
<br />
<br />
<table align="center" width="75%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    	<td width="25%">
        	<?php if($site['user']['permissions'][1]['view-events']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/events.php">Event\'s</a>');} ?>
        </td>
        <td width="25%">&nbsp;</td>
    	<td width="25%">
        	<?php if($site['user']['permissions'][1]['view-event-types']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/events.php?categorys">Category\'s</a>');} ?>
        </td>
    	<td width="25%">
        	<?php if($site['user']['permissions'][1]['add-event-types']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/events.php?add&category">Add Category</a>');} ?>
        </td>
	</tr>
</table>
<br />

<?php
if((isset($_GET['add'])) && (isset($_GET['category'])))
{	//Add Level
	?>
    <form action="<?php echo($site['url_path'] . '/' . $site['admin_path'] . '/events.php&category'); ?>" method="post" name="a-Addcategory">
    <table align="center" width="30%" border="0" cellspacing="0" cellpadding="0">
    	<tr>
        	<td colspan="2"><b>Add Category</b></td>
        </tr>
        <tr>
        	<td colspan="2">&nbsp;</td>
        </tr>
  		<tr>
    		<td>Name:</td>
        	<td><input type="text" name="name" id="name" /></td>
        </tr>
        <tr>
        	<td>Status:</td>
        	<td>
            <select name="status" id="status">
            	<option value="1" selected>Live</option>
                <option value="0">Draft</option>
            </select>
            </td>
        </tr>
        <tr>
        	<td><input type="submit" name="create-c" id="create-c" value="Create"></td>
            <td>&nbsp;</td>
        </tr>
    </table>
    </form>
    <?php
}
elseif((isset($_GET['edit'])) && (isset($_GET['event'])))
{	//Edit User
	$rs = mysql_query("SELECT * FROM events WHERE `id` = '" . mysql_real_escape_string($_GET['edit']) . "'", $conn);
	
	if(mysql_num_rows($rs) == 1)
	{
		$row = mysql_fetch_array($rs);
		?>
        <form action="<?php echo($site['url_path'] . '/' . $site['admin_path'] . '/events.php'); ?>" method="post" name="a-Edituser">
        <table align="center" width="30%" border="0" cellspacing="0" cellpadding="0">
        	<tr>
                <td colspan="2"><b>Edit Level</b></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td>Name:</td>
                <td><input type="text" name="name" id="name" value="<?php echo($row['name']); ?>" />
                	<input type="hidden" name="id" id="id" value="<?php echo($row['id']); ?>" />
                </td>
            </tr>
            <tr>
                <td>Event Type:</td>
                <td>
                <select name="type" id="type">
                    <?php
                    $rs2 = mysql_query("SELECT * FROM evnType WHERE `status` = '1'", $conn);
                    while($row2 = mysql_fetch_array($rs2))
                    {
                        echo('<option value="' . $row2['id'] . '"');
						if($row['type'] == $row2['id']){echo(' selected');}
						echo('>' . $row2['name'] . '</option>');
                    }
                    ?>
                </select>
                </td>
            </tr>
            <tr>
                <td>Start Date:</td>
                <td><input type="text" name="startDate" id="startDate" value="<?php echo($row['startDate']);?>" /></td>
            </tr>
            <tr>
                <td>End Date:</td>
                <td><input type="text" name="endDate" id="endDate" value="<?php echo($row['endDate']); ?>" /></td>
            </tr>
            <tr>
                <td>Details:</td>
                <td><textarea name="details" cols="45" rows="5" id="details" /><?php echo($row['details']); ?></textarea></td>
            </tr>
            <tr>
                <td>Organizer:</td>
                <td>
                <select name="org" id="org">
                    <?php
                    $rs3 = mysql_query("SELECT `id`, `username`, `fullname` FROM users", $conn);
                    while($row3 = mysql_fetch_array($rs3))
                    {
                        echo('<option value="' . $row3['id'] . '"');
						if($row['id'] == $row3['id']){echo(' selected');}
						echo('>' . $row3['username'] . ' (' . $row3['fullname'] . ')</option>');
                    }
                    ?>
                </select>
                </td>
            </tr>
            <tr>
                <td><input type="submit" name="change-e" id="change-e" value="Change"></td>
                <td>&nbsp;</td>
            </tr>
        </table>
        </form>
    	<?php
	}
	else
	{
		box_t('Events');
		echo('No event found.');
		echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/events.php" /> ');
		box_b();
	}
}
elseif((isset($_GET['edit'])) && (isset($_GET['category'])))
{	//Add Level
	$rs = mysql_query("SELECT * FROM evnType WHERE `id` = '" . $_GET['edit'] . "'", $conn);
	
	if(mysql_num_rows($rs))
	{
		$row = mysql_fetch_array($rs);
		?>
		<form action="<?php echo($site['url_path'] . '/' . $site['admin_path'] . '/events.php'); ?>" method="post" name="a-Editcategory">
		<table align="center" width="30%" border="0" cellspacing="0" cellpadding="0">
        	<tr>
                <td colspan="2"><b>Edit Category</b></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
			<tr>
				<td>Name:</td>
				<td><input type="text" name="name" id="name" value="<?php echo($row['name']); ?>" />
					<input type="hidden" name="id" id="id" value="<?php echo($row['id']); ?>" />
				</td>
			</tr>
			<tr>
				<td>Status:</td>
				<td>
				<select name="status" id="status">
					<option value="1"<?php if($row['status'] == '1'){echo(' selected');} ?>>Live</option>
					<option value="0"<?php if($row['status'] == '0'){echo(' selected');} ?>>Draft</option>
				</select>
				</td>
			</tr>
			<tr>
				<td><input type="submit" name="change-c" id="change-c" value="Change"></td>
				<td>&nbsp;</td>
			</tr>
		</table>
		</form>
		<?php
	}
	else
	{
		box_t('Events');
		echo('No user found.');
		echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/events.php?categorys" /> ');
		box_b();
	}
}
elseif(isset($_GET['categorys']))
{	//View Levels
	$rs = mysql_query("SELECT * FROM evnType", $conn);
	
	?>
    <table align="center" class="sortable" width="75%" border="0" cellspacing="0" cellpadding="0">
    	<tr>
        	<td><b>ID</b></td>
            <td><b>Name</b></td>
            <td><b>Status</b></td>
        </tr>
        <?php
		while($row = mysql_fetch_array($rs))
		{
		?>
        <tr>
        	<td><?php echo($row['id']); ?></td>
            <td><?php echo($row['name']); ?></td>
            <td><?php if($row['status'] == 1){echo('Live');}else{echo('Draft');} ?></td>
            <td>
            	<?php if($site['user']['permissions'][1]['edit-event-types']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/events.php?edit=' . $row['id'] . '&category"><img src="' . $site['url_path'] . '/images/icons/edit.gif" title="Edit" alt="Edit" border="0"></a>');} ?>
            </td>
            <td>
            	<?php if($site['user']['permissions'][1]['remove-event-types']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/events.php?remove=' . $row['id'] . '&category"><img src="' . $site['url_path'] . '/images/icons/delete.gif" title="Remove" alt="Remove" border="0"></a>');} ?>
            </td>
        </tr>
        <?php
		}
		?>
    <?php
	echo('</table>');
}
else
{	//Main Page
	$rs = mysql_query("SELECT * FROM events", $conn);
	
	?>
    <table align="center" class="sortable" width="75%" border="0" cellspacing="0" cellpadding="0">
  		<tr>
    		<td><b>ID</b></td>
    		<td><b>Name</b></td>
   			<td><b>Start Date</b></td>
    		<td><b>End Date</b></td>
            <td><b>Type</b></td>
            <td><b>Organizer</b></td>
            <td><b>Participants</b></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
  		</tr>
    	<?php
		while($row = mysql_fetch_array($rs))
		{
			$rs2 = mysql_query("SELECT `name` FROM evnType WHERE `id` = '" . $row['type'] . "'", $conn);
			$row2 = mysql_fetch_array($rs2);
			
			$rs3 = mysql_query("SELECT `username` FROM users WHERE `id` = '" . $row['org'] . "'", $conn);
			$row3 = mysql_fetch_array($rs3);
		?>
  		<tr>
    		<td><?php echo($row['id']); ?></td>
    		<td><?php echo($row['name']); ?></td>
    		<td><?php echo($row['startDate']); ?></td>
    		<td><?php echo($row['endDate']); ?></td>
            <td><?php echo($row2['name']); ?></td>
            <td><?php echo($row3['username']); ?></td>
            <td><?php echo($row['par']); ?></td>
            <td>
				<?php if($site['user']['permissions'][1]['edit-events']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/events.php?edit=' . $row['id'] . '&event"><img src="' . $site['url_path'] . '/images/icons/edit.gif" title="Edit" alt="Edit" border="0"></a>');} ?>
            </td>
            <td>
            	<?php if($site['user']['permissions'][1]['remove-events']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/events.php?remove=' . $row['id'] . '&event"><img src="' . $site['url_path'] . '/images/icons/delete.gif" title="Remove" alt="Remove" border="0"></a>');} ?>
            </td>
  		</tr>
        <?php
	}
	echo('</table>');
}
footer_b();
?>