<?php
include_once('../includes/settings.inc.php');
include_once('../includes/connection.php');
include_once('../includes/functions.inc.php');
start_uControl();
logged_in('admin');
buld_permissions();
page_permissions(1, 'view-bank');
$admin_header = true;
$sortTable = true;
include_once('../designs/header.php');

if(isset($_GET['remove']))
{
	$rs = mysql_query("DELETE FROM baccount WHERE `id` = '" . mysql_real_escape_string($_GET['remove']) . "'", $conn);
}
elseif(isset($_POST['add']))
{
	$eCheck = array();
	
	for($i=1; $i<=5; $i++)
	{
		if(($_POST['amount-' . $i] == NULL) || ($_POST['memo-' . $i] == NULL))
		{
			$eCheck[$i] = true;
		}
		else
		{
			$eCheck[$i] = false;
			$update++;
		}
	}
	
	if($update > 0)
	{
		for($i=1; $i<=5; $i++)
		{
			if(!$eCheck[$i])
			{
				$sql = "INSERT INTO baccount (`amount`, `memo`, `userId`, `by`, `action`, `date`) VALUES 
						('" . mysql_real_escape_string($_POST['amount-' . $i]) . "', '" . mysql_real_escape_string($_POST['memo-' . $i]) . "', '" . mysql_real_escape_string($_POST['to-' . $i]) . "', '" . mysql_real_escape_string($_POST['by-' . $i]) . "', '" . mysql_real_escape_string($_POST['action-' . $i]) . "', '" . mysql_real_escape_string($_POST['date-' . $i]) . "')";
		
				$rs = mysql_query($sql, $conn);
			}
		}
		
		build_bank_balance();
		
		echo('<br /><br />');
		box_t('Bank');
		echo('Money has been added.');
		echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/bank.php" /> ');
		box_b();
	}
	else
	{
		echo('<br /><br />');
		box_t('Bank');
		echo('Money could not be added.');
		box_b();
	}
}
elseif(isset($_POST['change']))
{
	if(($_POST['amount'] == NULL) || ($_POST['memo'] == NULL))
	{
		$eCheck = true;
	}
	
	if(!isset($eCheck))
	{
		$sql = "UPDATE baccounts SET `amount` = '" . mysql_real_escape_string($_POST['amount']) . "', `memo` = '" . mysql_real_escape_string($_POST['memo']) . "', `userId` = '" . mysql_real_escape_string($_POST['to']) . "', `by` = '" . mysql_real_escape_string($_POST['by']) . "', `date` = '" . mysql_real_escape_string($_POST['date']) . "' WHERE `id` = '" . mysql_real_escape_string($_POST['id']) . "'";
		$rs = mysql_query($sql, $conn);
		echo('<br /><br />');
		box_t('Bank');
		echo('Transaction has been updated.');
		echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/bank.php" /> ');
		box_b();
	}
	else
	{
		echo('<br /><br />');
		box_t('Bank');
		echo('Transaction could not be updated.');
		box_b();
	}
}
?>
<br />
<br />
<table align="center" width="75%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    	<td><?php if($site['user']['permissions'][1]['add-money']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/bank.php?add">Add Money</a>');} ?></td>
    	<td>&nbsp;</td>
    	<td>&nbsp;</td>
    	<td>&nbsp;</td>
	</tr>
</table>
<br />
<?php
if((!isset($_GET['add'])) && (!isset($_GET['edit'])) && (!isset($_GET['user'])))
{	//Main Page
	$rs = mysql_query("SELECT `balance` FROM users", $conn);
	while($row = mysql_fetch_array($rs)){$balance = $balance + $row['balance'];}
	$rs2 = mysql_query("SELECT COUNT(*) AS total FROM baccount", $conn);
	$trans = mysql_result($rs2, 0, "total");
	?>
	<table align="center" width="60%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="33%"><b>Balance:</b> $<?php echo($balance); ?></td>
			<td width="33%"><b>Transactions:</b> <?php echo($trans); ?></td>
			<td width="33%"><b></b></td>
		</tr>
	</table>
	<br />
	<?php

	$rs3 = mysql_query("SELECT * FROM baccount", $conn);
	?>
    <table align="center" class="sortable" width="75%" border="0" cellspacing="0" cellpadding="0">
  		<tr>
    		<td><b>ID</b></td>
    		<td><b>To</b></td>
   			<td><b>By</b></td>
    		<td><b>Date</b></td>
            <td><b>Memo</b></td>
            <td><b>Action</b></td>
            <td><b>Amount</b></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
  		</tr>
    	<?php
		while($row3 = mysql_fetch_array($rs3))
		{
			$rs4 = mysql_query("SELECT `username` FROM users WHERE `id` = '" . $row3['userId'] . "'", $conn);
			$row4 = mysql_fetch_array($rs4);
			$rs5 = mysql_query("SELECT `username` FROM users WHERE `id` = '" . $row3['by'] . "'", $conn);
			$row5 = mysql_fetch_array($rs5);
		?>
  		<tr>
    		<td><?php echo($row3['id']); ?></td>
    		<td><?php echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/bank.php?user=' . $row3['userId'] . '" alt="View ' . $row4['username'] . '">' . $row4['username'] . '</a>'); ?></td>
            <td><?php echo($row5['username']); ?></td>
    		<td><?php echo(substr($row3['date'], 0, 10)); ?></td>
            <td><?php echo($row3['memo']); ?></td>
            <td><?php if($row3['action']=='+'){echo('Credit');}else{echo('Debit');} ?></td>
            <td>$<?php echo($row3['amount']); ?></td>
            <td>
            	<?php if($site['user']['permissions'][1]['edit-bank']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/bank.php?edit=' . $row3['id'] . '"><img src="' . $site['url_path'] . '/images/icons/edit.gif" title="Edit" alt="Edit" border="0"></a>');} ?>
            </td>
            <td>
            	<?php if($site['user']['permissions'][1]['remove-money']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/bank.php?remove=' . $row3['id'] . '"><img src="' . $site['url_path'] . '/images/icons/delete.gif" title="Remove" alt="Remove" border="0"></a>');}?>
            </td>
  		</tr>
        <?php
	}
	echo('</table>');
}
elseif(isset($_GET['add']))
{	//Add Money
	?>
    <form action="<?php echo($site['url_path'] . '/' . $site['admin_path'] . '/bank.php'); ?>" method="post" name="a-addMoney">
    <table align="center" width="75%" border="0" cellspacing="0" cellpadding="0">
		<tr>
        	<td colspan="2"><b>Add Money</b></td>
        </tr>
        <tr>
        	<td colspan="2">&nbsp;</td>
        </tr>
        <tr>
			<td><b>Amount</b></td>
			<td><b>Action</b></td>
			<td><b>Date</b></td>
			<td><b>Memo</b></td>
			<td><b>To</b></td>
            <td><b>From</b></td>
		</tr>
        <tr>
        	<td colspan="6">&nbsp;</td>
        </tr>
        <?php
        for($i=1; $i<=5; $i++)
		{
		?>
		<tr>
			<td>$<input type="text" name="amount-<?php echo($i); ?>" id="amount-<?php echo($i); ?>" /></td>
			<td>
            <select name="action-<?php echo($i); ?>" id="action-<?php echo($i); ?>">
            	<option value="+" selected>Credit</option>
                <option value="-">Debit</option>
            </select>
            </td>
			<td><input type="text" name="date-<?php echo($i); ?>" id="date-<?php echo($i); ?>" size="10" value="<?php echo(date('m/d/Y'));?>" /></td>
			<td><input type="text" name="memo-<?php echo($i); ?>" id="memo-<?php echo($i); ?>" /></td>
            <td>
            <select name="to-<?php echo($i); ?>" id="to-<?php echo($i); ?>">
                    <?php
                    $rs = mysql_query("SELECT * FROM users WHERE `level` > '0'", $conn);
                    while($row = mysql_fetch_array($rs))
                    {
						$rs2 = mysql_query("SELECT `name` FROM levels WHERE `id` = '" . $row['level'] . "'", $conn);
						$row2 = mysql_fetch_array($rs2);
						
                        echo('<option value="' . $row['id'] . '">' . $row['username'] . ' (' .  $row2['name'] . ')</option>');
                    }
                    ?>
            </select>
            </td>
			<td>
            <select name="by-<?php echo($i); ?>" id="by-<?php echo($i); ?>">
                    <?php
                   	$rs3 = mysql_query("SELECT * FROM users WHERE `level` > '0'", $conn);
                    while($row3 = mysql_fetch_array($rs3))
                    {
						$rs4 = mysql_query("SELECT `name` FROM levels WHERE `id` = '" . $row3['level'] . "'", $conn);
						$row4 = mysql_fetch_array($rs4);
						
                        echo('<option value="' . $row3['id'] . '"');
                        if($row3['id'] == $site['user']['id']){ echo(' selected'); }
                        echo('>' . $row3['username'] . ' (' .  $row4['name'] . ')</option>');
                    }
                    ?>
            </select>
            </td>
		</tr>
        <?php
		}
		?>
        <tr>
        	<td colspan="6"><input type="submit" name="add" id="add" value="Add" /></td>
        </tr>
	</table>
    </form>
    <?php
}
elseif(isset($_GET['edit']))
{	//Edit Transaction
	$rs = mysql_query("SELECT * FROM baccount WHERE `id` = '" . mysql_real_escape_string($_GET['edit']) . "'", $conn);
	
	if(mysql_num_rows($rs) == 1)
	{
		$row = mysql_fetch_array($rs);
		?>
        <form action="<?php echo($site['url_path'] . '/' . $site['admin_path'] . '/bank.php'); ?>" method="post" name="a-Edittransaction">
        <table align="center" width="30%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="2"><b>Edit Transaction</b></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td>ID:</td>
                <td><input name="id" type="text" id="id" value="<?php echo($row['id']); ?>" readonly="true" /></td>
            </tr>
            <tr>
                <td>To:</td>
                <td>
                <select name="to" id="to">
                    <?php
                    $rs2 = mysql_query("SELECT * FROM users WHERE `level` > '0'", $conn);
                    while($row2 = mysql_fetch_array($rs2))
                    {
						$rs3 = mysql_query("SELECT `name` FROM levels WHERE `id` = '" . $row2['level'] . "'", $conn);
						$row3 = mysql_fetch_array($rs3);
						
                        echo('<option value="' . $row2['id'] . '"');
                        if($row2['id'] == $row['userId']){ echo(' selected'); }
                        echo('>' . $row2['username'] . ' (' .  $row3['name'] . ')</option>');
                    }
                    ?>
                </select>
                </td>
            </tr>
            <tr>
                <td>By:</td>
                <td>
                <select name="by" id="by">
                    <?php
                    $rs4 = mysql_query("SELECT * FROM users WHERE `level` > '0'", $conn);
                    while($row4 = mysql_fetch_array($rs4))
                    {
						$rs5 = mysql_query("SELECT `name` FROM levels WHERE `id` = '" . $row4['level'] . "'", $conn);
						$row5 = mysql_fetch_array($rs5);
						
                        echo('<option value="' . $row4['id'] . '"');
                        if($row4['id'] == $row['by']){ echo(' selected'); }
                        echo('>' . $row4['username'] . ' (' .  $row5['name'] . ')</option>');
                    }
                    ?>
                </select>
                </td>
            </tr>
            <tr>
                <td>Amount:</td>
                <td><input type="text" name="amount" id="amount" value="<?php echo($row['amount']); ?>" /> ($0.00)</td>
            </tr>
            <tr>
                <td>Memo:</td>
                <td><input type="text" name="memo" id="memo" value="<?php echo($row['memo']); ?>" /></td>
            </tr>
            <tr>
                <td>Date:</td>
                <td><input type="text" name="date" id="date" value="<?php echo($row['date']); ?>" /> (mm/dd/yyyy)</td>
            </tr>
            <tr>
                <td>Action:</td>
                <td>
                <select name="action" id="action">
                	<option value="+"<?php if($row['action']=='+'){echo(' selected');}?>>Credit</option>
                    <option value="-"<?php if($row['action']=='-'){echo(' selected');}?>>Debit</option>
                </select>
                </td>
            </tr>
            <tr>
                <td><input type="submit" name="change" id="change" value="Change"></td>
                <td>&nbsp;</td>
            </tr>
        </table>
        </form>
    	<?php
	}
	else
	{
		box_t('Bank');
		echo('No transaction found.');
		echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/bank.php" /> ');
		box_b();
	}
}
elseif(isset($_GET['user']))
{	//View users Transaction
	$rs = mysql_query("SELECT * FROM users WHERE `id` = '" . mysql_real_escape_string($_GET['user']) . "'", $conn);
	
	if(mysql_num_rows($rs) == 1)
	{
		$row = mysql_fetch_array($rs);
		$rs2 = mysql_query("SELECT * FROM baccount WHERE `userId` = '" . $row['id'] . "'", $conn);
		?>
		<table align="center" width="60%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="33%"><b>Balance:</b> $<?php echo($row['balance']); ?></td>
				<td width="33%"><b>Transactions:</b> <?php if(mysql_num_rows($rs2)>0){echo(mysql_num_rows($rs2));}else{echo('0');} ?></td>
				<td width="33%"><b>Account Number:</b> 000<?php echo($row['id'] . '-' . date('Y')); ?></td>
			</tr>
		</table>
		<br />
		<?php
		if(mysql_num_rows($rs2) > 0)
		{	
			?>
			<table align="center" class="sortable" width="60%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><b>ID</b></td>
					<td><b>Date</b></td>
					<td><b>Memo</b></td>
					<td><b>Amount</b></td>
					<td><b>Sum</b></td>
				</tr>
				<?php
				$sum = 0.00;
				while($row2 = mysql_fetch_array($rs2))
				{
					?>
					<tr>
						<td><?php echo($row2['id']); ?></td>
						<td><?php echo($row2['date']); ?></td>
						<td><?php echo(substr($row2['memo'], 0, 25)); ?></td>
						<td><?php echo($row2['amount']); ?></td>
						<td><?php if($row2['action'] == '+'){$sum = $sum + $row2['amount'];}elseif($row2['action'] == '-'){$sum = $sum - $row2['amount'];} echo($sum); ?></td>
					</tr>
					<?php
				}
				?>
			</table>
			<?php
		}
		else
		{	//Nothing found. Sorry.
			box_t('Bank');
			?>
			No transactions found.
			<?php
			box_b();
		}
	}
	else
	{	//Nothing found. Sorry.
		box_t('Bank');
		?>
		No user found.
		<?php
		box_b();
	}
}
echo('</center>');
footer_b();
?>