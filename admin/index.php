<?php
include_once('../includes/settings.inc.php');
include_once('../includes/connection.php');
include_once('../includes/functions.inc.php');
start_uControl();
logged_in('admin');
buld_permissions();
page_permissions(1, 'admin-panel');
$admin_header = true;
include_once('../designs/header.php');
?>
<br />
<?php
if($site['user']['permissions'][1]['view-news-feed'])
{
	echo('<center><div class="aNews" id="global-title">Global News Feed</div>');

	$feed = sepFeed(grabFeed($site['feed']['g-url'], $site['mN']));
	if(sizeof($feed)>5){$size=4;}else{$size=sizeof($feed)-1;}
	if($size>0)
	{
		for($i=0; $i<=$size; $i++)
		{
			if(($feed[$i]['mType'] == 0) || ($site['user']['mType'] == $feed[$i]['mType']))
			{
				?>
                <div class="aNews round" id="oBox">
                    <div class="aNews" id="iBox">
                        <div class="aNews" id="title"><?php echo(cleanString($feed[$i]['title'], 'text-output')); ?></div>
                        <div class="aNews" id="by">By: <?php echo(cleanString($feed[$i]['by'], 'text-output')); ?></div>
                        <div class="aNews" id="date">Printed: <?php echo(cleanString($feed[$i]['date'], 'text-output')); ?></div>
                        <div class="dummy"></div>
                        <div>&nbsp;</div>
                        <div class="aNews" id="content"><?php echo(cleanString($feed[$i]['content'], 'text-output')); ?></div>
                    </div>
                </div>
				<br />
				<?php
			}
		}
		echo('</center>');
	}
	else
	{
		box_t('News Feed');
		echo('The news feed is currently empty.');
		box_b();
	}
}
else
{
	echo('Hello');
}

footer_b();
?>