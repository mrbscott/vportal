<?php
include_once('../includes/settings.inc.php');
include_once('../includes/connection.php');
include_once('../includes/functions.inc.php');
start_uControl();
logged_in('admin');
buld_permissions();
page_permissions(1, 'settings-view');
$admin_header = true;
$sortTable = true;
include_once('../designs/header.php');

if(isset($_POST['change']))
{	//Update area
	if(!isset($_GET['view'])){$section = 'site';}else{$section = cleanString($_GET['view'], 'text-input');}
	
	$rs = mysql_query("SELECT * FROM settings WHERE `section` = '" . $section . "'", $conn);
	if(mysql_num_rows($rs))
	{
		while($row = mysql_fetch_array($rs))
		{
			if($_POST[$row['name']] == NULL)
			{
				$eCheck = true;
			}
			else
			{
				$rs2 = mysql_query("UPDATE settings SET `value` = '" . $_POST[$row['name']] . "' WHERE `name` = '" . $row['name'] . "'", $conn);
			}
		}
		
		box_t('Settings');
		if(!isset($eCheck)){echo('Settings have been updated.');}else{echo('Settings may not have been updated.');}
		echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/settings.php?view=' . $section . '" /> ');
		box_b();
	}
	else
	{	//FAIL
		box_t('Settings');
		echo('No settings found which could be updated.');
		echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/settings.php" /> ');
		box_b();
	}
}

?>
<br />
<table align="center" width="75%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    	<td>
        	<?php if($site['user']['permissions'][1]['view-site-settings']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/settings.php">Site</a>');} ?>
        </td>
        <td>
        	<?php if($site['user']['permissions'][1]['view-mail-settings']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/settings.php?view=mail">Mail</a>');} ?>
        </td>
    	<td>
        	<?php if($site['user']['permissions'][1]['view-status-settings']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/settings.php?view=status">Status</a>');} ?>
        </td>
    	<td>
        	<?php if($site['user']['permissions'][1]['view-permissions-settings']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/settings.php?view=permissions">Permissions</a>');} ?>
        </td>
        <td>
        	<?php if($site['user']['permissions'][1]['view-ip-ban-settings']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/settings.php?view=ipBans">IP Ban\'s</a>');} ?>
        </td>
        <td>
        	<?php if($site['user']['permissions'][1]['view-misc-settings']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/settings.php?view=misc">Misc</a>');} ?>
        </td>
	</tr>
</table>
<br />
<br />
<?php
//Main Settings Page
if(!isset($_GET['view'])){$section = 'site';}else{$section = cleanString($_GET['view'], 'text-input');}

$rs = mysql_query("SELECT * FROM settings WHERE `section` = '" . $section . "' ORDER BY `id` ASC", $conn);
if(mysql_num_rows($rs))
{	//Yay... lets begin showing the settings.
	?>
    <table align="center" width="45%" border="0" cellspacing="0" cellpadding="0">
    	<form action="<?php echo($site['url_path'] . '/' . $site['admin_path'] . '/settings.php?view=' . $section); ?>" method="post" name="a-changeSettings">
	<?php
	while($row = mysql_fetch_array($rs))
	{
		?>
		<tr>
        	<td><?php echo($row['title']); ?>:</td>
            <td>
            	<?php
				if($row['input'] == 'text')
				{
					echo('<input type="text" name="' . $row['name'] . '" id="' . $row['name'] . '" value="' . $row['value'] . '" />');
				}
				elseif($row['input'] == 'select-r')
				{
					$options = explode('[*]', $row['options']);
					echo('<select name="' . $row['name'] . '" id="' . $row['name'] . '">');
					foreach($options as $key => $value)
					{
						echo('<option value="' . $key . '"');
						if($row['value'] == $key){echo(' selected');}
						echo('>' . $value . '</option>');
					}
					echo('</select>');
				}
				elseif($row['input'] == 'select-o')
				{
					$rs2 = mysql_query("SELECT `id`, `name` FROM " . $row['options'] . " WHERE `status` = '1' ORDER BY `name` ASC", $conn);
					echo('<select name="' . $row['name'] . '" id="' . $row['name'] . '">');
					while($row2 = mysql_fetch_array($rs2))
					{
						echo('<option value="' . $row2['id'] . '"');
						if($row['value'] == $row2['id']){echo(' selected');}
						echo('>' . $row2['name'] . '</option>');
					}
					echo('</select>');
				}
				elseif($row['input'] == 'textarea')
				{
					echo('<textarea cols="45" rows="5" name="' . $row['name'] . '" id="' . $row['name'] . '">' . $row['value'] . '</textarea>');
				}
				?>
            </td>
        </tr>
		<?php
	}
	?>
    	<tr>
        	<td colspan="2">&nbsp;</td>
    	</tr>
    	<tr>
        	<td colspan="2">
            	<input type="<?php
				if(($_GET['view'] == 'site') && ($site['user']['permissions'][1]['edit-site-settings'])){$submit=true;}
				elseif(($_GET['view'] == 'mail') && ($site['user']['permissions'][1]['edit-mail-settings'])){$submit=true;}
				elseif(($_GET['view'] == 'status') && ($site['user']['permissions'][1]['edit-status-settings'])){$submit=true;}
				elseif(($_GET['view'] == 'permissions') && ($site['user']['permissions'][1]['edit-permissions-settings'])){$submit=true;}
				elseif(($_GET['view'] == 'ipBans') && ($site['user']['permissions'][1]['edit-ip-ban-settings'])){$submit=true;}
				elseif(($_GET['view'] == 'misc') && ($site['user']['permissions'][1]['edit-misc-settings'])){$submit=true;}
				
                if(isset($submit))
				{
					echo('submit');
				}
				else
				{
					echo('button');
				}
				?>" name="change" id="change" value="Change" />
        	</td>
    	</tr>
    </form>
    </table>
    <?php
}
else
{	//Nothing Found
	box_t('Settings');
	echo('No settings found.');
	box_b();
}

footer_b();
?>