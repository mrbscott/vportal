<?php
include_once('../includes/settings.inc.php');
include_once('../includes/connection.php');
include_once('../includes/functions.inc.php');
start_uControl();
logged_in('admin');
buld_permissions();
page_permissions(1, 'view-members');
$admin_header = true;
$sortTable = true;
include_once('../designs/header.php');

if((isset($_GET['remove'])) && (isset($_GET['user'])))
{	//Remove User
	$rs = mysql_query("UPDATE users SET `level` = '0' WHERE `id` = '" . mysql_real_escape_string($_GET['remove']) . "'", $conn);
}
elseif((isset($_GET['remove'])) && (isset($_GET['level'])))
{	//Remove Level
	$rs = mysql_query("DELETE FROM levels WHERE `id` = '" . mysql_real_escape_string($_GET['remove']) . "'", $conn);
	$_GET['levels'] = true;
}
elseif(isset($_POST['create-u']))
{	//Create User
	if(($_POST['username'] == NULL) || ($_POST['password'] == NULL))
	{
		$eCheck = true;
	}
	
	if(!isset($eCheck))
	{
		$sql = "INSERT INTO users (`username`, `password`, `email`, `fullname`, `firstaid`, `level`, `mType`, `mmsn`, `ip`) VALUES 
									('" . mysql_real_escape_string($_POST['username']) . "', '" . hashPassword($_POST['password']) . "', '" . mysql_real_escape_string($_POST['email']) . "', '" . mysql_real_escape_string($_POST['fullname']) . "', '" . mysql_real_escape_string($_POST['firstaid']) . "', '" . mysql_real_escape_string($_POST['level']) . "', '" . mysql_real_escape_string($_POST['mType']) . "', '" . mysql_real_escape_string($_POST['mmsn']) . "', '" . mysql_real_escape_string($_POST['ip']) . "')";
		
		$rs = mysql_query($sql, $conn);
		echo('<br /><br />');
		box_t('Members');
		echo('User has been added.');
		echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/members.php" /> ');
		box_b();
	}
	else
	{
		echo('<br /><br />');
		box_t('Members');
		echo('User could not be added.');
		box_b();
	}
}
elseif(isset($_POST['create-l']))
{	//Create Level
	if($_POST['name'] == NULL)
	{
		$eCheck = true;
	}
	
	if(!isset($eCheck))
	{
		$hold_1 = explode(',', $site['up']);
		
		foreach($hold_1 as $value_2)
		{
			$hold_2 = explode('=>', $value_2);
			
			if($_POST['up-' . $hold_2['0']])
			{
				$per .= $hold_2['0'] . '=>1,';
			}
			else
			{
				$per .= $hold_2['0']  . '=>0,';
			}
		}
		
		$per = substr($per, 0, strlen($per)-1);
		
		$per .= '[*]';
		
		$hold_1 = explode(',', $site['ap']);
		
		foreach($hold_1 as $value_2)
		{
			$hold_2 = explode('=>', $value_2);
			
			if($_POST['ap-' . $hold_2['0']])
			{
				$per .= $hold_2['0'] . '=>1,';
			}
			else
			{
				$per .= $hold_2['0']  . '=>0,';
			}
		}
		
		$per = substr($per, 0, strlen($per)-1);
		
		$sql = "INSERT INTO levels (`name`, `status`, `permissions`) VALUES ('" . mysql_real_escape_string($_POST['name']) . "', '" . mysql_real_escape_string($_POST['status']) . "', '" . $per . "')";
		
		$rs = mysql_query($sql, $conn);
		$_GET['levels'] = true;
		echo('<br /><br />');
		box_t('Members');
		echo('Level has been added.');
		echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/members.php?levels" /> ');
		box_b();
	}
	else
	{
		echo('<br /><br />');
		box_t('Members');
		echo('Level could not be added.');
		box_b();
	}
}
elseif(isset($_POST['change-u']))
{	//Change User
	if($_POST['username'] == NULL)
	{
		$eCheck = true;
	}
	
	if(!isset($eCheck))
	{
		$sql = "UPDATE users SET `username` = '" . mysql_real_escape_string($_POST['username']) . "'";
		if($_POST['password'] != NULL){ $sql .= " ,`password` = '" . hashPassword($_POST['password']) . "'"; }
		$sql .= ", `email` = '" . mysql_real_escape_string($_POST['email']) . "', `fullname` = '" . mysql_real_escape_string($_POST['fullname']) . "', `firstaid` = '" . mysql_real_escape_string($_POST['firstaid']) . "', `level` = '" . mysql_real_escape_string($_POST['level']) . "', `mType` = '" . mysql_real_escape_string($_POST['mType']) . "', `mmsn` = '" . mysql_real_escape_string($_POST['mmsn']) . "', `ip` = '" . mysql_real_escape_string($_POST['ip']) . "' WHERE `id` = '" . mysql_real_escape_string($_POST['id']) . "'"; 
		
		$rs = mysql_query($sql, $conn);
		echo('<br /><br />');
		box_t('Members');
		echo('User has been updated.');
		echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/members.php" /> ');
		box_b();
	}
	else
	{
		echo('<br /><br />');
		box_t('Members');
		echo('User could not be updated.');
		box_b();
	}
}
elseif(isset($_POST['change-l']))
{	//Change Level 
	if($_POST['name'] == NULL)
	{
		$eCheck = true;
	}
	
	if(!isset($eCheck))
	{
		$hold_1 = explode(',', $site['up']);
		
		foreach($hold_1 as $value_2)
		{
			$hold_2 = explode('=>', $value_2);
			
			if($_POST['up-' . $hold_2['0']])
			{
				$per .= $hold_2['0'] . '=>1,';
			}
			else
			{
				$per .= $hold_2['0']  . '=>0,';
			}
		}
		
		$per = substr($per, 0, strlen($per)-1);
		
		$per .= '[*]';
		
		$hold_1 = explode(',', $site['ap']);
		
		foreach($hold_1 as $value_2)
		{
			$hold_2 = explode('=>', $value_2);
			
			if($_POST['ap-' . $hold_2['0']])
			{
				$per .= $hold_2['0'] . '=>1,';
			}
			else
			{
				$per .= $hold_2['0']  . '=>0,';
			}
		}
		
		$per = substr($per, 0, strlen($per)-1);
				
		$rs = mysql_query("UPDATE levels SET `name` = '" . mysql_real_escape_string($_POST['name']) . "', `status` = '" . mysql_real_escape_string($_POST['status']) . "', `permissions` = '" . $per . "' WHERE `id` = '" . mysql_real_escape_string($_POST['id']) . "'", $conn);
		
		echo('<br /><br />');
		$_GET['levels'] = true;
		box_t('Members');
		echo('Level has been changed.');
		echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/members.php?levels" /> ');
		box_b();
	}
	else
	{
		echo('<br /><br />');
		box_t('Members');
		echo('Level could not be changed.');
		box_b();
	}
}




//SPACE







?>
<br />
<br />
<table align="center" width="75%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    	<td width="25%">
        	<?php if($site['user']['permissions'][1]['view-members']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/members.php">User\'s</a>');} ?>
        </td>
        <td width="25%">
        	<?php if($site['user']['permissions'][1]['add-members']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/members.php?add&user">Add User</a>');} ?>
        </td>
    	<td width="25%">
        	<?php if($site['user']['permissions'][1]['view-levels']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/members.php?levels">Level\'s</a>');} ?>
        </td>
    	<td width="25%">
        	<?php if($site['user']['permissions'][1]['add-levels']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/members.php?add&level">Add Level</a>');} ?>
        </td>
	</tr>
</table>
<br />

<?php
if((isset($_GET['add'])) && (isset($_GET['user'])))
{	//Add User
	?>
    <form action="<?php echo($site['url_path'] . '/' . $site['admin_path'] . '/members.php'); ?>" method="post" name="a-Adduser">
    <table align="center" width="30%" border="0" cellspacing="0" cellpadding="0">
    	<tr>
        	<td colspan="2"><b>Add User</b></td>
        </tr>
        <tr>
        	<td colspan="2">&nbsp;</td>
        </tr>
  		<tr>
    		<td>Username:</td>
        	<td><input type="text" name="username" id="username" /></td>
        </tr>
        <tr>
        	<td>Password:</td>
        	<td><input type="password" name="password" id="password" /></td>
        </tr>
        <tr>
        	<td>Email:</td>
        	<td><input type="text" name="email" id="email" /></td>
       	</tr>
        <tr>
        	<td>Full Name:</td>
        	<td><input type="text" name="fullname" id="fullname" /></td>
        </tr>
        <tr>
        	<td>First Aid:</td>
        	<td><select name="firstaid" id="firstaid">
				<?php
				$rs = mysql_query("SELECT * FROM firstaid WHERE `status` = '1' ORDER BY `ID` ASC", $conn);
				while($row = mysql_fetch_array($rs))
				{
					echo('<option value="' . $row['id'] . '">' . $row['name'] . '</option>');
				}
            	?>
        	</select>
        	</td>
        </tr>
        <tr>
        	<td>Level:</td>
        	<td>
            <select name="level" id="level">
				<?php
				$rs = mysql_query("SELECT * FROM levels WHERE `status` = '1' ORDER BY `ID` ASC", $conn);
				while($row = mysql_fetch_array($rs))
				{
					echo('<option value="' . $row['id'] . '">' . $row['name'] . '</option>');
				}
            	?>
        	</select>
            </td>
        </tr>
        <tr>
        	<td>Membership Type:</td>
        	<td>
            <select name="mType" id="mType">
				<?php
				$rs = mysql_query("SELECT * FROM mTypes WHERE `status` = '1' ORDER BY `ID` ASC", $conn);
				while($row = mysql_fetch_array($rs))
				{
					echo('<option value="' . $row['id'] . '">' . $row['name'] . '</option>');
				}
            	?>
        	</select>
            </td>
        </tr>
        <tr>
        	<td>MMS Number:</td> 
        	<td><input type="text" name="mmsn" id="mmsn" /></td>
        </tr>
        <tr>
        	<td>IP:</td> 
        	<td><input type="text" name="ip" id="ip" /></td>
        </tr>
        <tr>
        	<td><input type="submit" name="create-u" id="create-u" value="Create"></td>
            <td>&nbsp;</td>
        </tr>
    </table>
    </form>
    <?php
}
elseif((isset($_GET['add'])) && (isset($_GET['level'])))
{	//Add Level
	?>
    <form action="<?php echo($site['url_path'] . '/' . $site['admin_path'] . '/members.php'); ?>" method="post" name="a-Addlevel">
    <table align="center" width="30%" border="0" cellspacing="0" cellpadding="0">
  		<tr>
        	<td colspan="2"><b>Add Level</b></td>
        </tr>
        <tr>
        	<td colspan="2">&nbsp;</td>
        </tr>
        <tr>
    		<td>Name:</td>
        	<td><input type="text" name="name" id="name" /></td>
        </tr>
        <tr>
        	<td>Status:</td>
        	<td>
            <select name="status" id="status">
            	<option value="1" selected>Live</option>
                <option value="0">Draft</option>
            </select>
            </td>
        </tr>
        <tr>
        	<td>User Permissions:</td>
        	<td>
            	<?php
				$hold_1 = explode(',', $site['up']);
					
				foreach($hold_1 as $value_2)
				{
					$hold_2 = explode('=>', $value_2);
					echo('<label><input name="up-' . $hold_2['0'] . '" type="checkbox" ');
					echo('/>' . $hold_2['1'] . '</label><br />');
				}
				?>
        	</td>
       	</tr>
        <tr>
        	<td>&nbsp;</td>
        </tr>
        <tr>
        	<td>Admin Permissions:</td>
        	<td>
            	<?php
				$hold_1 = explode(',', $site['ap']);
					
				foreach($hold_1 as $value_2)
				{
					$hold_2 = explode('=>', $value_2);
					echo('<label><input name="ap-' . $hold_2['0'] . '" type="checkbox" ');
					echo('/>' . $hold_2['1'] . '</label><br />');
				}
				?>
        	</td>
       	</tr>
        <tr>
        	<td><input type="submit" name="create-l" id="create-l" value="Create"></td>
            <td>&nbsp;</td>
        </tr>
    </table>
    </form>
    <?php
}
elseif((isset($_GET['edit'])) && (isset($_GET['user'])))
{	//Edit User
	$rs = mysql_query("SELECT * FROM users WHERE `id` = '" . mysql_real_escape_string($_GET['edit']) . "'", $conn);
	
	if(mysql_num_rows($rs) == 1)
	{
		$row = mysql_fetch_array($rs);
		?>
        <form action="<?php echo($site['url_path'] . '/' . $site['admin_path'] . '/members.php'); ?>" method="post" name="a-Edituser">
        <table align="center" width="30%" border="0" cellspacing="0" cellpadding="0">
        	<tr>
                <td colspan="2"><b>Edit User</b></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td>Username:</td>
                <td><input type="text" name="username" id="username" value="<?php echo($row['username']); ?>" />
                	<input type="hidden" name="id" id="id" value="<?php echo($row['id']); ?>" />
                </td>
            </tr>
            <tr>
                <td>Password:</td>
                <td><input type="password" name="password" id="password" /></td>
            </tr>
            <tr>
                <td>Email:</td>
                <td><input type="text" name="email" id="email" value="<?php echo($row['email']); ?>" /></td>
            </tr>
            <tr>
                <td>Full Name:</td>
                <td><input type="text" name="fullname" id="fullname" value="<?php echo($row['fullname']); ?>" /></td>
            </tr>
            <tr>
                <td>First Aid:</td>
                <td>
                <select name="firstaid" id="firstaid">
                    <?php
                    $rs2 = mysql_query("SELECT * FROM firstaid WHERE `status` = '1' ORDER BY `ID` ASC", $conn);
                    while($row2 = mysql_fetch_array($rs2))
                    {
                        echo('<option value="' . $row2['id'] . '"');
						if($row['firstaid'] == $row2['id']){echo(' selected');}
						echo('>' . $row2['name'] . '</option>');
                    }
                    ?>
                </select>
                </td>
            </tr>
            <tr>
                <td>Level:</td>
                <td>
                <select name="level" id="level">
                    <?php
                    $rs3 = mysql_query("SELECT * FROM levels WHERE `status` = '1' ORDER BY `ID` ASC", $conn);
                    while($row3 = mysql_fetch_array($rs3))
                    {
                        echo('<option value="' . $row3['id'] . '"');
						if($row['level'] == $row3['id']){echo(' selected');}
						echo('>' . $row3['name'] . '</option>');
                    }
                    ?>
                </select>
                </td>
            </tr>
            <tr>
        	<td>Membership Type:</td>
                <td>
                <select name="mType" id="mType">
                    <?php
                    $rs4 = mysql_query("SELECT * FROM mTypes WHERE `status` = '1' ORDER BY `ID` ASC", $conn);
                    while($row4 = mysql_fetch_array($rs4))
                    {
                        echo('<option value="' . $row4['id'] . '"');
						if($row['mType'] == $row4['id']){echo(' selected');}
						echo('>' . $row4['name'] . '</option>');
                    }
                    ?>
                </select>
                </td>
        	</tr>
            <tr>
                <td>MMS Number:</td> 
                <td><input type="text" name="mmsn" id="mmsn" value="<?php echo($row['mmsn']); ?>" /></td>
            </tr>
            <tr>
                <td>Balance:</td>
                <td><input type="text" name="balance" id="balance" value="$ <?php echo($row['balance']); ?>" readonly /></td>
            </tr>
            <tr>
                <td>IP:</td> 
                <td><input type="text" name="ip" id="ip" value="<?php echo($row['ip']); ?>" /></td>
            </tr>
            <tr>
                <td><input type="submit" name="change-u" id="change-u" value="Change"></td>
                <td>&nbsp;</td>
            </tr>
        </table>
        </form>
    	<?php
	}
	else
	{
		box_t('Members');
		echo('No user found.');
		echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/members.php" /> ');
		box_b();
	}
}
elseif((isset($_GET['edit'])) && (isset($_GET['level'])))
{	//Add Level
	$rs = mysql_query("SELECT * FROM levels WHERE `id` = '" . $_GET['edit'] . "'", $conn);
	
	if(mysql_num_rows($rs))
	{
		$row = mysql_fetch_array($rs);
		
		$per = explode('[*]', $row['permissions']);
					
		foreach($per as $key => $value_1)
		{
			$hold_1 = explode(',', $value_1);
			
			foreach($hold_1 as $value_2)
			{
				$hold_2 = explode('=>', $value_2);
				$perC[$key][$hold_2['0']] = $hold_2['1'];
			}
		}
		?>
		<form action="<?php echo($site['url_path'] . '/' . $site['admin_path'] . '/members.php'); ?>" method="post" name="a-Editlevel">
		<table align="center" width="30%" border="0" cellspacing="0" cellpadding="0">
        	<tr>
                <td colspan="2"><b>Edit Level</b></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
			<tr>
				<td>Name:</td>
				<td><input type="text" name="name" id="name" value="<?php echo($row['name']); ?>" />
					<input type="hidden" name="id" id="id" value="<?php echo($row['id']); ?>" />
				</td>
			</tr>
			<tr>
				<td>Status:</td>
				<td>
				<select name="status" id="status">
					<option value="1"<?php if($row['status'] == '1'){echo(' selected');} ?>>Live</option>
					<option value="0"<?php if($row['status'] == '0'){echo(' selected');} ?>>Draft</option>
				</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">User Permissions:</td>
            </tr>
            <tr>
            	<td></td>
				<td>
					<?php
					/*
					$upC = $per[0];
					$up = $site['up'];
					foreach($up as $key => $value)
					{
						echo('<label><input name="up-' . $key . '" type="checkbox" ');
						if($upC[$key]==1){echo('checked="checked "');}
						echo('/>' . $value . '</label><br />');
					}
					*/
				
					$hold_1 = explode(',', $site['up']);
					
					foreach($hold_1 as $value_2)
					{
						$hold_2 = explode('=>', $value_2);
						echo('<label><input name="up-' . $hold_2['0'] . '" type="checkbox" ');
						if($perC[0][$hold_2['0']] == 1){echo('checked="checked "');}
						echo('/>' . $hold_2['1'] . '</label><br />');
					}
					?>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">Admin Permissions:</td>
            </tr>
            <tr>
            	<td></td>
				<td>
					<?php
					/*
					$apC = $per[1];
					$ap = $site['ap'];
					foreach($ap as $key => $value)
					{
						echo('<label><input name="ap-' . $key . '" type="checkbox"');
						if($apC[$key]==1){echo(' checked="checked"');}
						echo(' />' . $value . '</label><br />');
					}
					*/
					
					$hold_1 = explode(',', $site['ap']);
					
					foreach($hold_1 as $value_2)
					{
						$hold_2 = explode('=>', $value_2);
						echo('<label><input name="ap-' . $hold_2['0'] . '" type="checkbox" ');
						if($perC[1][$hold_2['0']] == 1){echo('checked="checked "');}
						echo('/>' . $hold_2['1'] . '</label><br />');
					}
					?>
				</td>
			</tr>
			<tr>
				<td><input type="submit" name="change-l" id="change-l" value="Change"></td>
				<td>&nbsp;</td>
			</tr>
		</table>
		</form>
		<?php
	}
	else
	{
		box_t('Members');
		echo('No user found.');
		echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/members.php?levels" /> ');
		box_b();
	}
}
elseif(isset($_GET['levels']))
{	//View Levels
	$rs = mysql_query("SELECT * FROM levels", $conn);
	
	?>
    <table align="center" class="sortable" width="75%" border="0" cellspacing="0" cellpadding="0">
    	<tr>
        	<td><b>ID</b></td>
            <td><b>Name</b></td>
            <td><b>Status</b></td>
            <td><b>Permissions</b></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <?php
		while($row = mysql_fetch_array($rs))
		{
		?>
        <tr>
        	<td><?php echo($row['id']); ?></td>
            <td><?php echo($row['name']); ?></td>
            <td><?php if($row['status'] == 1){echo('Live');}else{echo('Draft');} ?></td>
            <td><?php echo($row['permissions']); ?></td>
            <td>
            	<?php if($site['user']['permissions'][1]['edit-levels']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/members.php?edit=' . $row['id'] . '&level"><img src="' . $site['url_path'] . '/images/icons/edit.gif" title="Edit" alt="Edit" border="0"></a>');} ?>
            </td>
            <td>
            	<?php if($site['user']['permissions'][1]['remove-levels']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/members.php?remove=' . $row['id'] . '&level"><img src="' . $site['url_path'] . '/images/icons/delete.gif" title="Remove" alt="Remove" border="0"></a>');} ?>
            </td>
        </tr>
        <?php
		}
		?>
    <?php
	echo('</table>');
}
else
{	//Main Page
	$rs = mysql_query("SELECT * FROM users", $conn);
	
	?>
    <table align="center" class="sortable" width="75%" border="0" cellspacing="0" cellpadding="0">
  		<tr>
    		<td><b>ID</b></td>
    		<td><b>Username</b></td>
   			<td><b>Email</b></td>
    		<td><b>Full Name</b></td>
            <td><b>Balance</b></td>
            <td><b>Level</b></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
  		</tr>
    	<?php
		while($row = mysql_fetch_array($rs))
		{
			$rs2 = mysql_query("SELECT `name` FROM levels WHERE `id` = '" . $row['level'] . "'", $conn);
			$row2 = mysql_fetch_array($rs2);
		?>
  		<tr>
    		<td><?php echo($row['id']); ?></td>
    		<td><?php echo($row['username']); ?></td>
    		<td><?php echo($row['email']); ?></td>
    		<td><?php echo($row['fullname']); ?></td>
            <td>$<?php echo($row['balance']); ?></td>
            <td><?php echo($row2['name']); ?></td>
            <td>
				<?php if($site['user']['permissions'][1]['edit-members']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/members.php?edit=' . $row['id'] . '&user"><img src="' . $site['url_path'] . '/images/icons/edit.gif" title="Edit" alt="Edit" border="0"></a>');} ?>
            </td>
            <td>
            	<?php if($site['user']['permissions'][1]['remove-members']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/members.php?remove=' . $row['id'] . '&user"><img src="' . $site['url_path'] . '/images/icons/delete.gif" title="Remove" alt="Remove" border="0"></a>');} ?>
            </td>
  		</tr>
        <?php
	}
	echo('</table>');
}
footer_b();
?>