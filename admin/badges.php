<?php
include_once('../includes/settings.inc.php');
include_once('../includes/connection.php');
include_once('../includes/functions.inc.php');
start_uControl();
logged_in('admin');
buld_permissions();
page_permissions(1, 'view-badge-tracking');
$admin_header = true;
$sortTable = true;
include_once('../designs/header.php');

if((isset($_GET['remove'])) && (isset($_GET['track'])))
{
	$rs = mysql_query("DELETE FROM badgeTracking WHERE `id` = '" . cleanString($_GET['remove'], 'text-input') . "'", $conn);
}
elseif((isset($_GET['remove'])) && (isset($_GET['badges'])))
{
	$rs = mysql_query("DELETE FROM badges WHERE `id` = '" . cleanString($_GET['remove'], 'text-input') . "'", $conn);
}
elseif(isset($_POST['create-t']))
{
	if(($_POST['user'] == NULL) || ($_POST['badge'] == NULL))
		{
			$eCheck = true;
		}
		
		if(!isset($eCheck))
		{
			$rs = mysql_query("INSERT INTO badgeTracking (`userId`, `badgeId`, `startDate`) VALUES ('" . cleanString($_POST['user'], 'text-input') . "', '" . cleanString($_POST['badge'], 'text-input') . "', '" . date('m/d/Y') . "')", $conn);
			
			$rs = mysql_query("SELECT `id` FROM badgeTracking WHERE `userId` = '" . cleanString($_POST['badge'], 'text-input') . "' AND `badgeId` = '" . cleanString($_POST['badge'], 'text-input') . "'", $conn);
			$row = mysql_fetch_array($rs);
	
			box_t('Badges');
			echo('Badge work has been created.');
			echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/badges.php" /> ');
			box_b();
		}
		else
		{
			box_t('Badges');
			echo('Badge work could not be created.');
			box_b();
		}
}
elseif(isset($_POST['change-t']))
{
	if($_POST['id'] == NULL)
	{
		$eCheck = true;
	}
	
	if(!isset($eCheck))
	{
		$rs = mysql_query("SELECT `id`, `badgeId` FROM badgeTracking WHERE `id` = '" . cleanString($_POST['id'], 'text-input') . "'", $conn);
		
		if(mysql_num_rows($rs))
		{
			$row = mysql_fetch_array($rs);
			
			$rs2 = mysql_query("SELECT `program` FROM badges WHERE `id` = '" . $row['badgeId'] . "'", $conn);
			$row2 = mysql_fetch_array($rs2);
				
			$program = explode('[*]', $row2['program']);
			$tp1 = sizeof($program)-1;
			$x=0;
			for($i=0; $i<=$tp1; $i++)
			{
				if($_POST['p-' . $i]){$progress .= '1';$x++;}else{$progress .= '0';}
			}
			
			$sql = "UPDATE badgeTracking SET `progress` = '" . $progress . "'";
			if($x==sizeof($program)){$sql .= ", `finished` = '1', `endDate` = '" . date('m/d/Y') . "'";}
			$sql .= " WHERE `id` = '" . $row['id'] . "'";
			$rs = mysql_query($sql, $conn);
			
			box_t('Badges');
			echo('Badge work has been changed.');
			echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/badges.php" /> ');
			box_b();
		}
		else
		{
			box_t('Badges');
			echo('Badge work could not be found.');
			box_b();
		}
	}
	else
	{
		box_t('Badges');
		echo('Badge work could not be changed.');
		box_b();
	}
}
elseif(isset($_POST['create-b']))
{
	if(($_POST['name'] == NULL) || ($_POST['description'] == NULL) || ($_POST['program'] == NULL) || ($_POST['mType'] == NULL) || ($_POST['status'] == NULL))
	{
		$eCheck = true;
	}
	
	if(!isset($eCheck))
	{
		$sql = "INSERT INTO badges (`name`, `description`, `program`, `mType`, `status`) VALUES ('" . mysql_real_escape_string($_POST['name']) . "', '" . mysql_real_escape_string($_POST['description']) . "', '" . mysql_real_escape_string($_POST['program']) . "', '" . mysql_real_escape_string($_POST['mType']) . "', '" . mysql_real_escape_string($_POST['status']) . "')";
		$rs = mysql_query($sql, $conn);
		
		box_t('Badges');
		echo('Badge added.');
		echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/badges.php?badges" /> ');
		box_b();
	}
	else
	{
		box_t('Badges');
		echo('Badge could not be added.');
		box_b();
	}
}
elseif(isset($_POST['change-b']))
{
	if(($_POST['name'] == NULL) || ($_POST['description'] == NULL) || ($_POST['program'] == NULL) || ($_POST['mType'] == NULL) || ($_POST['status'] == NULL))
	{
		$eCheck = true;
	}
	
	if(!isset($eCheck))
	{
		$sql = "UPDATE badges SET `name` = '" . cleanString($_POST['name'], 'text-input') . "', `description` = '" . cleanString($_POST['description'], 'text-input') . "', `program` = '" . cleanString($_POST['program'], 'text-input') . "', `mType` = '" . cleanString($_POST['mType'], 'text-input') . "', `status` = '" . cleanString($_POST['status'], 'text-input') . "' WHERE `id` = '" . cleanString($_POST['id'], 'text-input') . "'";
		$rs = mysql_query($sql, $conn);
		
		box_t('Badges');
		echo('Badge updated.');
		echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/badges.php?badges" /> ');
		box_b();
	}
	else
	{
		box_t('Badges');
		echo('Badge could not be updated.');
		box_b();
	}
}
?>
<br />
<br />
<table align="center" width="75%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    	<td>
        	<?php if($site['user']['permissions'][1]['view-badge-tracking']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/badges.php">Badge Tracking</a>');} ?>
        </td>
    	<td>
			<?php if($site['user']['permissions'][1]['add-badge-work']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/badges.php?add&track">Add Badge Work</a>');} ?>
        </td>
    	<td>
        	<?php if($site['user']['permissions'][1]['view-badges']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/badges.php?badges">Badges</a>');} ?>
        </td>
    	<td>
        	<?php if($site['user']['permissions'][1]['add-badge']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/badges.php?add&badges">Add Badge</a>');} ?>
        </td>
	</tr>
</table>
<br />

<?php
if((isset($_GET['edit'])) && (isset($_GET['track'])))
{	//Edit Badge Work
	$rs = mysql_query("SELECT * FROM badgeTracking WHERE `id` = '" . cleanString($_GET['edit'], 'text-input') . "'", $conn);
	
	if(mysql_num_rows($rs))
	{
		$row = mysql_fetch_array($rs);

		$rs2 = mysql_query("SELECT * FROM badges WHERE `id` = '" . $row['badgeId'] . "'", $conn);
		$row2 = mysql_fetch_array($rs2);
                
        $rs3 = mysql_query("SELECT `name` FROM mTypes WHERE `id` = '" . $row2['mType'] . "'", $conn);
		$row3 = mysql_fetch_array($rs3);
            
		?>
		<table align="center" width="45%" border="0" cellspacing="0" cellpadding="0">
			<form action="<?php echo($site['url_path'] . '/' . $site['admin_path']); ?>/badges.php" method="post" name="a-editBadge">
			<tr>
				<td colspan="2">
                    <b><?php echo($row2['name']); ?></b>
					<input type="hidden" name="id" id="id" value="<?php echo($row['id']); ?>" />
				</td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td>Description</td>
                <td><?php echo($row2['description']); ?></td>
            </tr>
            <tr>
               	<td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">Program</td>
            </tr>
            <?php
			$progress = $row['progress'];
			$program = explode('[*]', $row2['program']);
				
			foreach($program as $key => $value)
			{
				?>
                <tr>
                    <td><input type="checkbox" name="p-<?php echo($key); ?>"<?php if($progress[$key]){echo(' checked');}?> /><i>Completed</i></td>
					<td><?php echo($value); ?></td>
				</tr>
                <?php
            }
			?>
            <tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="change-t" id="change-t" value="Change" /></td>
			</tr>
			</form>
		</table>
    	<?php
	}
	else
	{	//No badge work found...
		box_t('Badges');
		echo('Badge work not found.');
		echo('<meta http-equiv="refresh" content="1;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/badges.php" /> ');
		box_b();
	}
}
elseif((isset($_GET['add'])) && (isset($_GET['track'])))
{	//Add Badge Work
	?>
    <table align="center" width="45%" border="0" cellspacing="0" cellpadding="0">
    	<form action="<?php echo($site['url_path'] . '/' . $site['admin_path']); ?>/badges.php" method="post" name="a-addTrack">
        <tr>
        	<td colspan="2"><b>Create Badge Work</b></td>
        </tr>
        <tr>
        	<td colspan="2">&nbsp;</td>
        </tr>
        <tr>
        	<td>User:</td>
            <td>
            	<select name="user" id="user">
					<?php
                    $rs = mysql_query("SELECT `id`, `username`, `fullname` FROM users ORDER BY `id` ASC", $conn);
                    
                    while($row = mysql_fetch_array($rs))
                    {
                        echo('<option value="' . $row['id'] . '">' . $row['username'] . ' (' . $row['fullname'] . ')</option>');
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
			<td>Badge:</td>
			<td>
				<select name="badge" id="badge">
					<?php
					$rs = mysql_query("SELECT `id`, `name` FROM mTypes ORDER BY `id` ASC", $conn);
                    
                    while($row = mysql_fetch_array($rs))
                    {
						$section[$row['id']] = $row['name'];
                    }
					
                    $rs2 = mysql_query("SELECT `id`, `name`, `mType` FROM badges ORDER BY `mType` ASC, `name` ASC", $conn);
                    
                    while($row2 = mysql_fetch_array($rs2))
                    {
                        echo('<option value="' . $row2['id'] . '">' . $row2['name'] . ' (' . $section[$row2['mType']] . ')</option>');
                    }
                    ?>
                </select>
            </td>
		</tr>
        <tr>
        	<td colspan="2">&nbsp;</td>
        </tr>
        <tr>
        	<td colspan="2"><input type="submit" name="create-t" id="create-t" value="Create" /></td>
        </tr>
        </form>
    </table>
    <?php
}
elseif((isset($_GET['edit'])) && (isset($_GET['badge'])))
{	//Edit Badge
	$rs = mysql_query("SELECT * FROM badges WHERE `id` = '" . mysql_real_escape_string($_GET['edit']) . "'", $conn);
	
	if(mysql_num_rows($rs))
	{
		$row = mysql_fetch_array($rs);
		?>
		<table align="center" width="45%" border="0" cellspacing="0" cellpadding="0">
			<form action="<?php echo($site['url_path'] . '/' . $site['admin_path']); ?>/badges.php?badges" method="post" name="a-editBadge">
			<tr>
				<td colspan="2"><b>Edit Badge</b></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td>Name:</td>
				<td>
                	<input type="text" name="name" id="name" value="<?php echo(cleanString($row['name'], 'text-output')); ?>" />
                    <input type="hidden" name="id" id="id" value="<?php echo($row['id']); ?>" />
                </td>
			</tr>
			<tr>
				<td>Description</td>
				<td><textarea cols="45" rows="5" name="description" id="description"><?php echo(cleanString($row['description'], 'text-output')); ?></textarea></td>
			</tr>
			<tr>
				<td>Program</td>
				<td><textarea cols="45" rows="5" name="program" id="program"><?php echo(cleanString($row['program'], 'text-output')); ?></textarea></td>
			</tr>
			<tr>
				<td>Section:</td>
				<td>
					<select name="mType" id="mType">
						<?php
						$rs2 = mysql_query("SELECT `id`, `name` FROM mTypes WHERE `status` = '1'", $conn);
						
						while($row2 = mysql_fetch_array($rs2))
						{
							echo('<option value="' . $row2['id'] . '"');
							if($row['mType'] == $row2['id']){echo(' selected');}
							echo('>' . $row2['name'] . '</option>');
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Status:</td>
				<td>
					<select name="status" id="status">
						<option value="1" <?php if($row['status']==1){echo(' selected');} ?>>Live</option>
						<option value="0" <?php if($row['status']==0){echo(' selected');} ?>>Draft</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="change-b" id="change-b" value="Change" /></td>
			</tr>
			</form>
		</table>
		<?php
	}
	else
	{	//Nothing Found...
		box_t('Badges');
		echo('Badge not found.');
		echo('<meta http-equiv="refresh" content="2;url=' . $site['url_path'] . '/' . $site['admin_path'] . '/badges.php?badges" /> ');
		box_b();
	}
}
elseif((isset($_GET['add'])) && (isset($_GET['badges'])))
{	//Add Badge
	?>
    <table align="center" width="45%" border="0" cellspacing="0" cellpadding="0">
    	<form action="<?php echo($site['url_path'] . '/' . $site['admin_path']); ?>/badges.php?badges" method="post" name="a-addBadge">
        <tr>
        	<td colspan="2"><b>Create Badge</b></td>
        </tr>
        <tr>
        	<td colspan="2">&nbsp;</td>
        </tr>
        <tr>
        	<td>Name:</td>
            <td><input type="text" name="name" id="name" /></td>
        </tr>
        <tr>
        	<td>Description</td>
            <td><textarea cols="45" rows="5" name="description" id="description"></textarea></td>
        </tr>
        <tr>
        	<td>Program</td>
            <td><textarea cols="45" rows="5" name="program" id="program"></textarea></td>
        </tr>
        <tr>
        	<td>Section:</td>
            <td>
            	<select name="mType" id="mType">
                	<?php
					$rs = mysql_query("SELECT `id`, `name` FROM mTypes WHERE `status` = '1'", $conn);
					
					while($row = mysql_fetch_array($rs))
					{
						echo('<option value="' . $row['id'] . '">' . $row['name'] . '</option>');
					}
					?>
                </select>
            </td>
        </tr>
        <tr>
        	<td>Status:</td>
            <td>
            	<select name="status" id="status">
                	<option value="1">Live</option>
            		<option value="0">Draft</option>
                </select>
            </td>
        </tr>
        <tr>
        	<td colspan="2"><input type="submit" name="create-b" id="create-b" value="Create" /></td>
        </tr>
        </form>
    </table>
    <?php
}
elseif(isset($_GET['badges']))
{	//Badges Page
	$rs = mysql_query("SELECT * FROM badges", $conn);
	
	?>
    <table align="center" class="sortable" width="75%" border="0" cellspacing="0" cellpadding="0">
  		<tr>
    		<td><b>ID</b></td>
    		<td><b>Name</b></td>
   			<td><b>Description</b></td>
            <td><b>Section</b></td>
    		<td><b>Status</b></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
  		</tr>
    	<?php
		while($row = mysql_fetch_array($rs))
		{
			$rs2 = mysql_query("SELECT `name` FROM mTypes WHERE `id` = '" . $row['mType'] . "'", $conn);
			$row2 = mysql_fetch_array($rs2);
			
			?>
			<tr>
				<td><?php echo($row['id']); ?></td>
				<td><?php echo($row['name']); ?></td>
				<td><?php echo(substr($row['description'],0,35)); ?>...</td>
                <td><?php echo($row2['name']); ?></td>
				<td><?php if($row['status']==1){echo('Live');}else{echo('Draft');} ?></td>
				<td>
					<?php if($site['user']['permissions'][1]['edit-badge']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/badges.php?edit=' . $row['id'] . '&badge"><img src="' . $site['url_path'] . '/images/icons/edit.gif" title="Edit" alt="Edit" border="0"></a>');} ?>
				</td>
				<td>
					<?php if($site['user']['permissions'][1]['remove-badge']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/badges.php?remove=' . $row['id'] . '&badge"><img src="' . $site['url_path'] . '/images/icons/delete.gif" title="Remove" alt="Remove" border="0"></a>');} ?>
				</td>
			</tr>
			<?php
		}
	echo('</table>');
}
else
{	//Main Page
	$rs = mysql_query("SELECT * FROM badgeTracking", $conn);
	
	?>
    <table align="center" class="sortable" width="75%" border="0" cellspacing="0" cellpadding="0">
  		<tr>
    		<td><b>ID</b></td>
    		<td><b>Badge</b></td>
   			<td><b>User</b></td>
            <td><b>Start Date</b></td>
            <td><b>End Date</b></td>
    		<td><b>Progress</b></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
  		</tr>
    	<?php
		while($row = mysql_fetch_array($rs))
		{
			$rs2 = mysql_query("SELECT `name`, `program` FROM badges WHERE `id` = '" . $row['badgeId'] . "'", $conn);
			$row2 = mysql_fetch_array($rs2);
			
			$rs3 = mysql_query("SELECT `fullname` FROM users WHERE `id` = '" . $row['userId'] . "'", $conn);
			$row3 = mysql_fetch_array($rs3);
			
			if($row['finished'] != 1)
			{
				$progress = $row['progress'];
				for($i=0;$i<=strlen($progress);$i++){if($progress[$i] == 1){$x++;}}
				if(($x>0)&&(strlen($progress)>0)){$progress=round(($x*100)/strlen($progress));}else{$progress=0;}
			}
			else
			{
				$progress = 100;
			}
			?>
			<tr>
				<td><?php echo($row['id']); ?></td>
				<td><?php echo($row2['name']); ?></td>
				<td><?php echo($row3['fullname']); ?></td>
                <td><?php echo($row['startDate']); ?></td>
                <td><?php echo($row['endDate']); ?></td>
				<td><?php echo($progress); ?>%</td>
				<td>
					<?php if($site['user']['permissions'][1]['edit-badge-work']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/badges.php?edit=' . $row['id'] . '&track"><img src="' . $site['url_path'] . '/images/icons/edit.gif" title="Edit" alt="Edit" border="0"></a>');} ?>
				</td>
				<td>
					<?php if($site['user']['permissions'][1]['remove-badge-work']){echo('<a href="' . $site['url_path'] . '/' . $site['admin_path'] . '/badges.php?remove=' . $row['id'] . '&track"><img src="' . $site['url_path'] . '/images/icons/delete.gif" title="Remove" alt="Remove" border="0"></a>');} ?>
				</td>
			</tr>
			<?php
		}
	echo('</table>');
}
footer_b();
?>