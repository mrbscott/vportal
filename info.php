<?php
include_once('./includes/settings.inc.php');
include_once('./includes/connection.php');
include_once('./includes/functions.inc.php');
start_uControl();
if($site['user']['login-status'])
{
	logged_in();
	buld_permissions();
}
$sideBarOff = true;
include_once('./designs/header.php');
?>
<br /><br />
<table align="center" width="75%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    	<td colspan="3">
			<font class="credits" id="title">vPortal</font><br />
			<b>Version:</b> <?php echo($site['vPortal']['version']); ?><br />
		</td>
	</tr>
    <tr>
    	<td colspan="3">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="3"><b>Description</b></td>
    </tr>
    <tr>
    	<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td colspan="2">
        	vPortal is a unique portal developed for groups to manage communications between members and leaders. vPortal was developed with the understanding that the web is the most important median which can be utilized to date. With this understanding vPortal takes on many vital roles within a group.<br />
        	<br />
			This includes;<br />
            - Information Feed<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A simple news blog which allows people to direct information at certain groups or to everyone.<br />
            - Profiles<br />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The use of profiles allows each person to set specific settings to their liking, provide specific information to their age.<br />
            - Events<br />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Share, Join, Enjoy - a easy to use system which allows a person to share an event, join in on an event, and enjoy the event. Events can include camps, activities, or what ever you can think of.<br />
            - Bank<br />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;It's a growing trend that groups divide money up amongst its members to allow for members to spend it as they see fit on appropriate things. The bank allows for members to see their account and the transactions which have taken place.<br />
            - Mail<br />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Chit chat here, chit chat there - it's all right here. A useful and easy way to carry a conversation on between friends while on the portal.<br />
            - Badge <i>(Coming Soon!)</i><br />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;As a key aspect to the program at any level, badges are fundamental and always enjoyable to achieve and receive. When and if you forget where you are in the program, all you need to do is check up here. It's also a great resource for learning about each badge and it's uses.<br />
            - Forums <i>(Coming Soon!)</i><br />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;When it comes to discussing small topics or reviewing important reminders. A forum is always a simple way to help share your thoughts. But on a national scale; it becomes a community for anyone who cares to learn and share.<br />
    		- Administrate<br />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;It's of the utmost importance to be able to administrate a portal in a quick, simple, and effective manner. It's with this in mind, the admin panel was developed to control the portal.
        </td>
    </tr>
    <tr>
    	<td colspan="3">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="3"><b>History</b></td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
		<td colspan="2">
			vPortal was first thought of many many years before it became a reality. It was through the growing needs of groups that it was developed. Finally after days of hard work, it has come to be a fully operational system.
        </td>
    </tr>
    <tr>
    	<td colspan="3">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="3"><b>Developed By</b></td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
    	<td>Brandon Scott</td>
        <td>Programmer</td>
    </tr>
    <tr>
    	<td colspan="3">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="3">
        	<b>Special Thanks</b>
        </td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
        <td colspan="2">Joe Collinson, Kayla Blakney, Andy Laya, Johnathan Silvko</td>
    </tr>
    <tr>
    	<td colspan="3">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="3">
        	<b>Copyrights</b>
        </td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
        <td colspan="2">
            vPortal Copyright &copy; 2005-<?php echo(date('Y')); ?> Brandon Scott. All Rights Reserved.
            <br /><br />
            <a href="http://scouts.ca">Scouts Canada</a> Copyright &copy; <?php echo(date('Y')); ?>. Scouts Canada Logo is registered trademarks of Scouts Canada.<br />
            <a href="http://scout.org">World Scout Bureau</a> Copyright &copy; <?php echo(date('Y')); ?>. Scouting Symbol is a registered trademarks of World Scout Bureau.<br />
            <a href="http://www.kryogenix.org/code/browser/sorttable/">SortTable</a> Copyright &copy; 2008 Stuart Langridge.<br />
            <a href="mailto:jason@onehackoranother.com">Boxy</a> Copyright &copy; 2008 Jason Frame<br />
            <a href="http://www.fckeditor.net">FCKeditor</a> Copyright &copy; 2003-2009 Frederico Caldeira Knabben
        </td>
    </tr>
    <tr>
    	<td colspan="3">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="3">
        	<b>Licenses</b>
        </td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
        <td colspan="2">A copy of the licenses for each copyrighted work can be found within the folder "licenses" included with this software. Questions and concerns should be directed to vPortal's software team.</td>
    </tr>
</table>
<?php footer_b(); ?>